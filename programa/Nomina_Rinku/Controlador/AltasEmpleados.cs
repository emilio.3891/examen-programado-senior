﻿using System;
using System.Collections.Generic;
using System.Data;
using Conector;
using Conexion;
using Datos;

namespace Controlador
{
    /// <summary>
    ///     Clase para el manejo de la informacion consultada  relacionadas a altas empleados
    /// </summary>
    public class AltasEmpleados
    {
        private readonly Sql _sql;

        public AltasEmpleados()
        {
            _sql = SqlFactory.MakeSql(
                SqlFactory.TypeSql.SqlServer,
                new ConnectionStrings("rinku"));
        }

        /// <summary>
        ///     Procedimiento para obtener un nuevo numero de empleado
        /// </summary>
        /// <returns>retorna una estructura nueva con el nuevo para la opcion</returns>
        public cat_empleados Proc_ConseguirNuevoEmpleado()
        {
            var dt = _sql.ConsultarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_nuevoEmpleado",
                Parametros = new List<Parametro>()
            });
            var empleados = new cat_empleados
            {
                num_empleado = Convert.ToInt32(dt.Rows[0][0].ToString()),
                nom_empleado = string.Empty,
                CatEmpleadosDetalles = new cat_empleadosDetalles
                {
                    num_empleado = Convert.ToInt32(dt.Rows[0][0].ToString()),
                    idu_rol = 1,
                    idu_tipoEmpleado = 1,
                    clv_imss = false,
                    clv_infonavit = false
                }
            };
            return empleados;
        }

        /// <summary>
        ///     retorna un lista los empleados disponibles filtrados por los criterios de los sp
        /// </summary>
        /// <param name="empleados">
        ///     estructura de los datos para filtrar el contenido se
        ///     buscara numero de empleado especifico y buscara con like en nombre
        /// </param>
        /// <returns>Retorna lista con los empleados buscados</returns>
        public List<cat_empleados> Proc_buscarEmpleado(cat_empleados empleados)
        {
            var list = new List<cat_empleados>();
            var dt = _sql.ConsultarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_buscarEmpleado",
                Parametros = new List<Parametro>
                {
                    new Parametro {Nombre = "@num_empleado", Valor = empleados.num_empleado},
                    new Parametro {Nombre = "@nom_empleado", Valor = empleados.nom_empleado}
                }
            });
            foreach (DataRow row in dt.Rows)
                list.Add(new cat_empleados
                {
                    num_empleado = Convert.ToInt32(row[0]),
                    nom_empleado = row[1].ToString(),
                    CatEmpleadosDetalles = new cat_empleadosDetalles
                    {
                        num_empleado = Convert.ToInt32(row[0]),
                        idu_tipoEmpleado = Convert.ToInt32(row[2]),
                        idu_rol = Convert.ToInt32(row[3]),
                        clv_imss = Convert.ToBoolean(Convert.ToInt32(row[4])),
                        clv_infonavit = Convert.ToBoolean(Convert.ToInt32(row[5]))
                    }
                });

            return list;
        }

        /// <summary>
        ///     Procedimientos para consultar roles disponibles
        /// </summary>
        /// <returns>Retorna lista con los roles almacenados en el servidor</returns>
        public List<cat_roles> Proc_consultarRoles()
        {
            var list = new List<cat_roles>();
            var dt = _sql.ConsultarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_consultarRoles",
                Parametros = new List<Parametro>()
            });

            foreach (DataRow row in dt.Rows)
                list.Add(new cat_roles
                {
                    idu_rol = Convert.ToInt32(row[0]),
                    des_rol = row[1].ToString()
                });

            return list;
        }

        /// <summary>
        ///     Procedimieto para consultar los diversos tipos de empleados
        /// </summary>
        /// <returns>Retorna lista con los diversos tipos de empleados almacenados en el servidor</returns>
        public List<cat_tiposEmpleados> Proc_consultarTiposEmpleados()
        {
            var list = new List<cat_tiposEmpleados>();
            var dt = _sql.ConsultarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_consultarTiposEmpleados",
                Parametros = new List<Parametro>()
            });

            foreach (DataRow row in dt.Rows)
                list.Add(new cat_tiposEmpleados
                {
                    idu_tipoEmpleado = Convert.ToInt32(row[0]),
                    des_tipoEmpleado = row[1].ToString()
                });

            return list;
        }

        /// <summary>
        ///     Guarda o actualiza el empleado envido en el servidor
        /// </summary>
        /// <param name="empleado">estructura con los datos de los empledos a guardar/actualizar</param>
        /// <returns>numero de filas afectadas</returns>
        public int Proc_GuardarEmpleado(cat_empleados empleado)
        {
            var filas = _sql.EjecutarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_guardarEmpleado",
                Parametros = new List<Parametro>
                {
                    new Parametro {Nombre = "@num_empleado", Valor = empleado.num_empleado},
                    new Parametro {Nombre = "@nom_empleado", Valor = empleado.nom_empleado},
                    new Parametro
                    {
                        Nombre = "@idu_tipoEmpleado",
                        Valor = empleado.CatEmpleadosDetalles.idu_tipoEmpleado
                    },
                    new Parametro {Nombre = "@idu_rol", Valor = empleado.CatEmpleadosDetalles.idu_rol},
                    new Parametro {Nombre = "@clv_imss", Valor = empleado.CatEmpleadosDetalles.clv_imss},
                    new Parametro {Nombre = "@clv_infonavit", Valor = empleado.CatEmpleadosDetalles.clv_infonavit}
                }
            });
            return filas;
        }
    }
}