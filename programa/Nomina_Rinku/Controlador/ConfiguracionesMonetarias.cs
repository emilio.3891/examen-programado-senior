﻿using System;
using System.Collections.Generic;
using System.Data;
using Conector;
using Conexion;
using Datos;

namespace Controlador
{
    /// <summary>
    ///     Clase para el manejo de la informacion consultada  relacionadas a configuraciones monetarias
    /// </summary>
    public class ConfiguracionesMonetarias
    {
        private readonly Sql _sql;

        /// <summary>
        ///     Clase para el manejo de la informacion consultada  relacionadas a configuraciones monetarias
        /// </summary>
        public ConfiguracionesMonetarias()
        {
            _sql = SqlFactory.MakeSql(
                SqlFactory.TypeSql.SqlServer,
                new ConnectionStrings("rinku"));
        }

        /// <summary>
        ///     Metodo para buscar la configuracion monetria
        /// </summary>
        /// <param name="confMonetarias">estructura con la descripcion a buscar</param>
        /// <returns>Lista con las configuraciones coincidentes a buscadas</returns>
        public List<cat_confMonetarias> Proc_buscarConfigMonetaria(cat_confMonetarias confMonetarias)
        {
            var list = new List<cat_confMonetarias>();
            var dt = _sql.ConsultarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_buscarConfigMonetaria",
                Parametros = new List<Parametro>
                {
                    new Parametro {Nombre = "@des_confMonetaria", Valor = confMonetarias.des_confMonetaria}
                }
            });

            foreach (DataRow row in dt.Rows)
                list.Add(new cat_confMonetarias
                {
                    idu_confMonetaria = Convert.ToInt16(row[0]),
                    clv_valor = Convert.ToDecimal(row[1].ToString()),
                    des_confMonetaria = row[2].ToString()
                });

            return list;
        }

        /// <summary>
        ///     Metodo para guardar los cambios a la configuracion monetarias
        /// </summary>
        /// <param name="confMonetarias">Estructuras con los datos a almacenar</param>
        /// <returns>Retorna el nuemero de filas afectadas</returns>
        public int Proc_GuardarConfigMonetaria(cat_confMonetarias confMonetarias)
        {
            var filas = _sql.EjecutarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_guardarConfigMonetaria",
                Parametros = new List<Parametro>
                {
                    new Parametro {Nombre = "@idu_confMonetaria", Valor = confMonetarias.idu_confMonetaria},
                    new Parametro {Nombre = "@clv_valor", Valor = confMonetarias.clv_valor}
                }
            });
            return filas;
        }
    }
}