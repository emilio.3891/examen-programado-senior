﻿using System;
using System.Collections.Generic;
using System.Data;
using Conector;
using Conexion;
using Datos;

namespace Controlador
{
    /// <summary>
    ///     Clase para el manejo de la informacion consultada  relacionadas a configuraciones del sistema
    /// </summary>
    public class ConfiguracionesSistemas
    {
        private readonly Sql _sql;

        /// <summary>
        ///     Clase para el manejo de la informacion consultada  relacionadas a configuraciones del sistema
        /// </summary>
        public ConfiguracionesSistemas()
        {
            _sql = SqlFactory.MakeSql(
                SqlFactory.TypeSql.SqlServer,
                new ConnectionStrings("rinku"));
        }

        /// <summary>
        ///     Metodo para buscar la configuracion del sistema
        /// </summary>
        /// <param name="confSistemas">estructura con la descripcion a buscar</param>
        /// <returns>Lista con las configuraciones coincidentes a buscadas</returns>
        public List<cat_confSistemas> Proc_buscarConfigSistema(cat_confSistemas confSistemas)
        {
            var list = new List<cat_confSistemas>();
            var dt = _sql.ConsultarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_buscarConfigSistema",
                Parametros = new List<Parametro>
                {
                    new Parametro {Nombre = "@idu_confSistemas", Valor = confSistemas.idu_confSistemas},
                    new Parametro {Nombre = "@des_confSistema", Valor = confSistemas.des_confSistema}
                }
            });

            foreach (DataRow row in dt.Rows)
                list.Add(new cat_confSistemas
                {
                    idu_confSistemas = Convert.ToInt16(row[0]),
                    opc_activo = Convert.ToBoolean(Convert.ToInt16(row[1].ToString())),
                    des_confSistema = row[2].ToString()
                });
            return list;
        }

        /// <summary>
        ///     Metodo para guardar los cambios a la configuracion del sistema
        /// </summary>
        /// <param name="confSistemas">Estructuras con los datos a almacenar</param>
        /// <returns>Retorna el nuemero de filas afectadas</returns>
        public int Proc_GuardarConfigSistema(cat_confSistemas confSistemas)
        {
            var filas = _sql.EjecutarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_guardarConfigSistema",
                Parametros = new List<Parametro>
                {
                    new Parametro {Nombre = "@idu_confSistemas", Valor = confSistemas.idu_confSistemas},
                    new Parametro {Nombre = "@opc_activo", Valor = confSistemas.opc_activo}
                }
            });
            return filas;
        }
    }
}