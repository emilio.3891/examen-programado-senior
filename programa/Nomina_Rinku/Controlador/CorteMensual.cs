﻿using System.Collections.Generic;
using Conector;
using Conexion;

namespace Controlador
{
    public class CorteMensual
    {
        private readonly Sql _sql;

        public CorteMensual()
        {
            _sql = SqlFactory.MakeSql(
                SqlFactory.TypeSql.SqlServer,
                new ConnectionStrings("rinku"));
        }

        public int Proc_corteMensual()
        {
            var filas = _sql.EjecutarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_corteMensual",
                Parametros = new List<Parametro>()
            });
            return filas;
        }
    }
}