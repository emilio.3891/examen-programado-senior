﻿using System;
using System.Collections.Generic;
using System.Data;
using Conector;
using Conexion;
using Datos;

namespace Controlador
{
    /// <summary>
    ///     Clase para el manejo de la informacion consultada  relacionadas a la captura de movimientos
    /// </summary>
    public class Movimientos
    {
        private readonly Sql _sql;

        /// <summary>
        ///     Clase para el manejo de la informacion consultada  relacionadas a la captura de movimientos
        /// </summary>
        public Movimientos()
        {
            _sql = SqlFactory.MakeSql(
                SqlFactory.TypeSql.SqlServer,
                new ConnectionStrings("rinku"));
        }

        /// <summary>
        ///     Procedimientos para buscar los detalles del empleado
        /// </summary>
        /// <param name="emp">estructura con el empleado a buscar los detalles</param>
        /// <returns>Retorna estructura con el detalle del empleado buscado</returns>
        public cat_empleados Proc_buscarEmpleadoDetalle(cat_empleados emp)
        {
            var dt = _sql.ConsultarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_buscarEmpleadoDetalle",
                Parametros = new List<Parametro>
                {
                    new Parametro {Nombre = "@num_empleado", Valor = emp.num_empleado}
                }
            });

            foreach (DataRow row in dt.Rows)
            {
                emp.nom_empleado = row["nom_empleado"].ToString();
                emp.CatEmpleadosDetalles.idu_tipoEmpleado = Convert.ToInt32(row["idu_tipoEmpleado"]);
                emp.CatEmpleadosDetalles.des_tipoEmpleado = row["des_tipoEmpleado"].ToString();
                emp.CatEmpleadosDetalles.idu_rol = Convert.ToInt32(row["idu_rol"]);
                emp.CatEmpleadosDetalles.des_rol = row["des_rol"].ToString();
            }

            return emp;
        }

        /// <summary>
        ///     Metodo para buscar los diversos tipos de turnos en el servidor
        /// </summary>
        /// <returns>Lista con los turnos en el servidor</returns>
        public List<cat_turnos> Proc_consultarTurnos()
        {
            var turnos = new List<cat_turnos>();
            var dt = _sql.ConsultarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_consultarTurnos",
                Parametros = new List<Parametro>()
            });

            foreach (DataRow row in dt.Rows)
                turnos.Add(new cat_turnos
                {
                    idu_turno = Convert.ToInt32(row["idu_turno"]),
                    des_turno = row["des_turno"].ToString()
                });

            return turnos;
        }

        /// <summary>
        ///     Consulta los movientos de la fecha enviada
        /// </summary>
        /// <param name="fecha">Fecha con la cual se realizara la busqueda</param>
        /// <returns>Lista con los diversos movimientos del la fecha buscada</returns>
        public List<CapturaMovimientos> Proc_ConsultarMovimentos(DateTime fecha)
        {
            var mov = new List<CapturaMovimientos>();
            var dt = _sql.ConsultarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_ConsultarMovimentos",
                Parametros = new List<Parametro> {new Parametro {Nombre = "@fecha", Valor = fecha}}
            });

            foreach (DataRow row in dt.Rows)
                mov.Add(new CapturaMovimientos
                {
                    fec_movimiento = Convert.ToDateTime(row["mov_nominaDetalles"]),
                    num_empleado = Convert.ToInt32(row["num_empleado"]),
                    nom_empleado = row["nom_empleado"].ToString(),
                    idu_rol = Convert.ToInt32(row["idu_rol"]),
                    des_rol = row["des_rol"].ToString(),
                    hrsTrabajadas = Convert.ToInt32(row["hrsTrabajadas"]),
                    numEntregas = Convert.ToInt32(row["numEntregas"]),
                    idu_turno = Convert.ToInt32(row["idu_turno"]),
                    hrsExtras = Convert.ToInt32(row["hrsExtras"])
                });
            return mov;
        }

        /// <summary>
        ///     Guarda los movientos de la estructura enviada
        /// </summary>
        /// <param name="mov">Movientos a guardar</param>
        /// <returns>Filas afectadas</returns>
        public int Proc_guardarMovimientos(CapturaMovimientos mov)
        {
            var filas = _sql.EjecutarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_guardarMovimientos",
                Parametros = new List<Parametro>
                {
                    new Parametro {Nombre = "@num_empleado", Valor = mov.num_empleado},
                    new Parametro {Nombre = "@fec_movimiento", Valor = mov.fec_movimiento},
                    new Parametro {Nombre = "@hrsTrabajadas", Valor = mov.hrsTrabajadas},
                    new Parametro {Nombre = "@hrsExtras", Valor = mov.hrsExtras},
                    new Parametro {Nombre = "@numEntregas", Valor = mov.numEntregas},
                    new Parametro {Nombre = "@idu_turno", Valor = mov.idu_turno}
                }
            });

            return filas;
        }
    }
}