﻿using System;
using System.Collections.Generic;
using System.Data;
using Conector;
using Conexion;
using Datos;

namespace Controlador
{
    /// <summary>
    ///     Clase para adaptara para el manejo de datos relaciondos a reportes
    /// </summary>
    public class Reportes
    {
        private readonly Sql _sql;

        /// <summary>
        ///     Clase para adaptara para el manejo de datos relaciondos a reportes
        /// </summary>
        public Reportes()
        {
            _sql = SqlFactory.MakeSql(
                SqlFactory.TypeSql.SqlServer,
                new ConnectionStrings("rinku"));
        }

        /// <summary>
        ///     Procedimientos para consultar el detalle genral de un determinado rango de fechas
        /// </summary>
        /// <param name="fechaInicio">Fecha inicio</param>
        /// <param name="fechaFinal">Fecha final</param>
        /// <returns>Estructura con los datos consultadaos</returns>
        public List<EstadisticasGenerales> Proc_ConsultarEstadisticaGeneral(DateTime fechaInicio, DateTime fechaFinal)
        {
            var estadisticasGenerales = new List<EstadisticasGenerales>();
            var dt = _sql.ConsultarSp(new ProcedimientoAlmacenado
            {
                Nombre = "proc_ConsultarEstadisticaGeneral",
                Parametros = new List<Parametro>
                {
                    new Parametro {Nombre = "@fechaInicio", Valor = fechaInicio},
                    new Parametro {Nombre = "@fechaFinal", Valor = fechaFinal}
                }
            });

            foreach (DataRow row in dt.Rows)
                estadisticasGenerales.Add(new EstadisticasGenerales
                {
                    num_empleado = row["num_empleado"].ToString(),
                    nom_empleado = row["nom_empleado"].ToString(),
                    num_mes = row["num_mes"].ToString(),
                    num_anio = row["num_anio"].ToString(),
                    fec_corte = Convert.ToDateTime(row["fec_corte"].ToString()).ToString("dd/MM/yyyy"),
                    imp_subTotal = row["imp_subTotal"].ToString(),
                    imp_total = row["imp_total"].ToString(),
                    des_estatus = row["des_estatus"].ToString()
                });

            return estadisticasGenerales;
        }
    }
}