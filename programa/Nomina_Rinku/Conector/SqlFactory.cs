﻿using System;
using Conexion;

namespace Conector
{
    /// <summary>
    ///     Factoria para conexiones de tipo sql
    /// </summary>
    public static class SqlFactory
    {
        /// <summary>
        ///     Enumeracion de los tipos de servidores soportados
        /// </summary>
        public enum TypeSql
        {
            SqlServer
        }

        /// <summary>
        ///     Metodo para retorna el tipo de servidor necesario utilizando la abstraccion y el debilitamiento de datos
        ///     para generalizar la clases
        /// </summary>
        /// <param name="typeSql">Tipo de servidor</param>
        /// <param name="connectionStrings">Cadena de conexion</param>
        /// <returns>Retorna tipo de dato sql para las consultas al servidor este dato esta abstraido</returns>
        public static Sql MakeSql(TypeSql typeSql, ConnectionStrings connectionStrings)
        {
            switch (typeSql)
            {
                case TypeSql.SqlServer:
                    return new SqlServer(connectionStrings.Connection);
                default:
                    //throw new ArgumentOutOfRangeException(nameof(typeSql), typeSql, null);
                    throw new Exception("Gestor invalido)");
            }
        }
    }
}