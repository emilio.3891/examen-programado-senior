﻿using System.Data;

namespace Conector
{
    /// <summary>
    ///     Clase para el manejo e interraccion de sql
    /// </summary>
    public abstract class Sql
    {
        /// <summary>
        ///     valor que contendra la conexion
        /// </summary>
        protected string Conexion;

        /// <summary>
        ///     Constructor para la clase de interracion sql
        /// </summary>
        /// <param name="conexion">cadena para la conexion sql</param>
        protected Sql(string conexion)
        {
            Conexion = conexion;
        }

        /// <summary>
        ///     Metodo para abrir la conexion a al servidor
        /// </summary>
        protected abstract void Conectar();

        /// <summary>
        ///     Metodo para ejecutar y consultar un sp que retorna una estructura de tipo tabla
        /// </summary>
        /// <param name="sp">estructura con los valores del sp a ejecutar</param>
        /// <returns>Retorna una tabla la informacion que retorna el sp</returns>
        public abstract DataTable ConsultarSp(ProcedimientoAlmacenado sp);

        /// <summary>
        ///     Metodo para ejecutar un sp
        /// </summary>
        /// <param name="sp">estructura con los valores del sp a ejecutar</param>
        /// <returns>el total de filas afectadas</returns>
        public abstract int EjecutarSp(ProcedimientoAlmacenado sp);

        /// <summary>
        ///     Metodo para desconectar del serivdor
        /// </summary>
        protected abstract void Desconectar();
    }
}