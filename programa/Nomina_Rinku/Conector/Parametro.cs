﻿namespace Conector
{
    /// <summary>
    ///     Estructura para ingresar parametros para sp
    /// </summary>
    public class Parametro
    {
        /// <summary>
        ///     Nombre del parametro @name
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        ///     Valor para del parametro del sp ej. 1, "Nombre", "YYYY/MM/DD"
        /// </summary>
        public object Valor { get; set; }
    }
}