﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Conector
{
    /// <summary>
    ///     Clase que implementa la coonexion a SQLServer
    /// </summary>
    internal class SqlServer : Sql
    {
        private readonly SqlConnection _con;
        private SqlDataAdapter _adapter;
        private SqlCommand _cmd;

        public SqlServer(string conexion)
            : base(conexion)
        {
            _con = new SqlConnection(conexion);
        }

        protected override void Desconectar()
        {
            if (_con.State == ConnectionState.Open) _con.Close();
        }

        public override DataTable ConsultarSp(ProcedimientoAlmacenado sp)
        {
            var dt = new DataTable();
            Conectar();
            using (_cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.CommandText = sp.Nombre;

                foreach (var item in sp.Parametros)
                    _cmd.Parameters.AddWithValue(item.Nombre, item.Valor ?? DBNull.Value);

                using (_adapter = new SqlDataAdapter(_cmd))
                {
                    _adapter.Fill(dt);
                }
            }

            Desconectar();

            return dt;
        }

        public override int EjecutarSp(ProcedimientoAlmacenado sp)
        {
            var filasAfectadas = 0;
            Conectar();
            using (_cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.CommandText = sp.Nombre;
                foreach (var item in sp.Parametros) _cmd.Parameters.AddWithValue(item.Nombre, item.Valor);
                filasAfectadas = _cmd.ExecuteNonQuery();
            }

            Desconectar();

            return filasAfectadas;
        }

        protected override void Conectar()
        {
            if (_con.State == ConnectionState.Closed) _con.Open();
        }
    }
}