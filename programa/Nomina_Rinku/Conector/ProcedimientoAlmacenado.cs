﻿using System.Collections.Generic;

namespace Conector
{
    /// <summary>
    ///     Estructura para un sp
    /// </summary>
    public class ProcedimientoAlmacenado
    {
        /// <summary>
        ///     Lista de parametros que contendra el sp
        /// </summary>
        public List<Parametro> Parametros { get; set; }

        /// <summary>
        ///     Nombre del sp
        /// </summary>
        public string Nombre { get; set; }
    }
}