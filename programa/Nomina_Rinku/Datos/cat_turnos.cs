﻿namespace Datos
{
    /// <summary>
    ///     Estructura con los atributos de las tablas en el servidor
    /// </summary>
    public class cat_turnos
    {
        public int idu_turno { get; set; }

        public string des_turno { get; set; }
    }
}