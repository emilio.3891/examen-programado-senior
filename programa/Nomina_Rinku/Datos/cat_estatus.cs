﻿namespace Datos
{
    /// <summary>
    ///     Estructura con los atributos de las tablas en el servidor
    /// </summary>
    public class cat_estatus
    {
        public int idu_estatus { get; set; }

        public string des_estatus { get; set; }
    }
}