﻿namespace Datos
{
    /// <summary>
    ///     Estructura con los atributos de las tablas en el servidor
    /// </summary>
    public class EstadisticasGenerales
    {
        public string num_empleado { get; set; }
        public string nom_empleado { get; set; }
        public string num_mes { get; set; }
        public string num_anio { get; set; }
        public string fec_corte { get; set; }
        public string imp_subTotal { get; set; }
        public string imp_total { get; set; }
        public string des_estatus { get; set; }
    }
}