﻿namespace Datos
{
    /// <summary>
    ///     Estructura con los atributos de las tablas en el servidor
    /// </summary>
    public class cat_empleadosDetalles
    {
        public int num_empleado { get; set; }

        public int? idu_tipoEmpleado { get; set; }

        public int? idu_rol { get; set; }

        public bool? clv_imss { get; set; }

        public bool? clv_infonavit { get; set; }

        public string des_tipoEmpleado { get; set; }

        public string des_rol { get; set; }
    }
}