﻿namespace Datos
{
    /// <summary>
    ///     Estructura con los atributos de las tablas en el servidor
    /// </summary>
    public class cat_tiposEmpleados
    {
        public int idu_tipoEmpleado { get; set; }

        public string des_tipoEmpleado { get; set; }
    }
}