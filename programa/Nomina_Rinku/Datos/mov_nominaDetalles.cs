﻿using System;

namespace Datos
{
    /// <summary>
    ///     Estructura con los atributos de las tablas en el servidor
    /// </summary>
    public class mov_nominaDetalles
    {
        public int num_empleado { get; set; }

        public DateTime fec_movimiento { get; set; }

        public int hrsTrabajadas { get; set; }

        public int hrsExtras { get; set; }

        public int numEntregas { get; set; }

        public int idu_turno { get; set; }

        public bool clv_procesado { get; set; }
    }
}