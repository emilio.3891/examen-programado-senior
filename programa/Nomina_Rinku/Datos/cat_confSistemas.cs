﻿namespace Datos
{
    /// <summary>
    ///     Estructura con los atributos de las tablas en el servidor
    /// </summary>
    public class cat_confSistemas
    {
        public int? idu_confSistemas { get; set; }

        public bool opc_activo { get; set; }

        public string des_confSistema { get; set; }

        public bool modificado { get; set; }
    }
}