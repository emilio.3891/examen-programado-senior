﻿using System;

namespace Datos
{
    /// <summary>
    ///     Estructura con los atributos de las tablas en el servidor
    /// </summary>
    public class CapturaMovimientos
    {
        public DateTime fec_movimiento { get; set; }

        public int num_empleado { get; set; }

        public string nom_empleado { get; set; }

        public int idu_rol { get; set; }

        public string des_rol { get; set; }

        public int hrsTrabajadas { get; set; }

        public int idu_turno { get; set; }

        public int numEntregas { get; set; }

        public int hrsExtras { get; set; }

        public bool modificado { get; set; }
    }
}