﻿namespace Datos
{
    /// <summary>
    ///     Estructura con los atributos de las tablas en el servidor
    /// </summary>
    public class cat_rolesDetalles
    {
        public int idu_rol { get; set; }

        public int idu_detalle { get; set; }

        public int idu_confMonetaria { get; set; }
    }
}