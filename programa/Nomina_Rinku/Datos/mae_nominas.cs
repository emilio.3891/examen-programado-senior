﻿using System;

namespace Datos
{
    /// <summary>
    ///     Estructura con los atributos de las tablas en el servidor
    /// </summary>
    public class mae_nominas
    {
        public int num_empleado { get; set; }

        public DateTime fec_corte { get; set; }

        public int idu_estatus { get; set; }

        public string num_mes { get; set; }

        public string num_anio { get; set; }

        public decimal imp_base { get; set; }

        public decimal imphrsExtra { get; set; }

        public decimal imp_entregas { get; set; }

        public decimal imp_bonos { get; set; }

        public decimal imp_isr { get; set; }

        public decimal imp_isrAdicional { get; set; }

        public decimal imp_Imss { get; set; }

        public decimal imp_Infonavit { get; set; }

        public decimal imp_subTotal { get; set; }

        public decimal imp_total { get; set; }
    }
}