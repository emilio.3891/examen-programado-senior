﻿namespace Datos
{
    /// <summary>
    ///     Estructura con los atributos de las tablas en el servidor
    /// </summary>
    public class cat_confMonetarias
    {
        /// <summary>
        ///     Estructura con los atributos de las tablas en el servidor
        /// </summary>
        public cat_confMonetarias()
        {
            modificado = false;
        }

        public int? idu_confMonetaria { get; set; }

        public decimal clv_valor { get; set; }

        public string des_confMonetaria { get; set; }

        public bool modificado { get; set; }
    }
}