﻿namespace Datos
{
    /// <summary>
    ///     Estructura con los atributos de las tablas en el servidor
    /// </summary>
    public class cat_empleados
    {
        public int? num_empleado { get; set; }

        public string nom_empleado { get; set; }

        public cat_empleadosDetalles CatEmpleadosDetalles { get; set; }
    }
}