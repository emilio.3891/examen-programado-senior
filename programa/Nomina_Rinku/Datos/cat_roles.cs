﻿namespace Datos
{
    /// <summary>
    ///     Estructura con los atributos de las tablas en el servidor
    /// </summary>
    public class cat_roles
    {
        public int idu_rol { get; set; }

        public string des_rol { get; set; }
    }
}