﻿namespace Mensajes
{
    /// <summary>
    ///     Clase con las cadenas para mensajes
    /// </summary>
    public static class Mensaje
    {
        public static string Exito { get { return "Se ha realizado la acción satisfactoriamente"; } }
        public static string Actualizado { get { return "Se ha actualizado la información satisfactoriamente"; } }
        public static string NoActualizado { get { return "No se ha podido actualizar la información"; } }
        public static string Guardado { get { return "Se a guardado los datos satisfactoriamente"; } }
        public static string NoGuardado { get { return "Ocurrió un error al guardar los datos proporcionados"; } }
        public static string Error { get { return "Ocurrió un error. Favor de volver a intentar"; } }
        public static string Advertencia { get { return "Verificar al realizar esta acción"; } }
        public static string FavorLlenarCampos { get { return "Favor de llenar los campo obligatorios"; } }
    }
}