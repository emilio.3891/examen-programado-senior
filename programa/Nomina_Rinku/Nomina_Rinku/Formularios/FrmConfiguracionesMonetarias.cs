﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Controlador;
using Datos;
using Nomina_Rinku.Herramientas;

namespace Nomina_Rinku.Formularios
{
    public partial class FrmConfiguracionesMonetarias : Form
    {
        private readonly ConfiguracionesMonetarias _configuracionesMonetarias;
        private List<cat_confMonetarias> _listConf;

        public FrmConfiguracionesMonetarias()
        {
            InitializeComponent();
            _configuracionesMonetarias = new ConfiguracionesMonetarias();
        }

        private void Buscar()
        {
            try
            {
                var catConfMonetarias = new cat_confMonetarias
                {
                    des_confMonetaria = txtDescripcionBusqueda.Text
                };

                BuscarConfMonetarias(catConfMonetarias);
            }
            catch (Exception exception)
            {
                Log.Add(exception.Message);
                MensajesEmergentes.MensajeError();
            }
        }

        private void BuscarConfMonetarias(cat_confMonetarias catConfMonetarias)
        {
            _listConf = _configuracionesMonetarias.Proc_buscarConfigMonetaria(catConfMonetarias);

            dgvConfiguracionesMonetarias.AutoGenerateColumns = false;
            dgvConfiguracionesMonetarias.Columns[0].DataPropertyName = "idu_confMonetaria";
            dgvConfiguracionesMonetarias.Columns[1].DataPropertyName = "clv_valor";
            dgvConfiguracionesMonetarias.Columns[2].DataPropertyName = "des_confMonetaria";
            dgvConfiguracionesMonetarias.DataSource = _listConf;
        }

        private void FrmConfiguracionesMonetarias_Load(object sender, EventArgs e)
        {
            Buscar();
        }

        private void txtDescripcionBusqueda_TextChanged(object sender, EventArgs e)
        {
            if (txtDescripcionBusqueda.TextLength > 3 || txtDescripcionBusqueda.TextLength == 0) Buscar();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Buscar();
        }

        private void Guardar(cat_confMonetarias conf)
        {
            _configuracionesMonetarias.Proc_GuardarConfigMonetaria(conf);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (MensajesEmergentes.DeseaGuardar())
            {
                try
                {
                    foreach (var catConfMonetarias in _listConf)
                        if (catConfMonetarias.modificado)
                            _configuracionesMonetarias.Proc_GuardarConfigMonetaria(catConfMonetarias);
                    MensajesEmergentes.MensajeActualizado();
                }
                catch (Exception exception)
                {
                    Log.Add(exception.Message);
                    MensajesEmergentes.MensajeNoActualizado();
                }
            }
        }

        private void dgvConfiguracionesMonetarias_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (_listConf != null) _listConf[e.RowIndex].modificado = true;
        }

        private void dgvConfiguracionesMonetarias_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            var view = (DataGridView) sender;
            e.Cancel = true;
            MensajesEmergentes.MensajeAdvertencia();
        }

        private void txtDescripcionBusqueda_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar) && !Char.IsWhiteSpace(e.KeyChar));
        }
    }
}