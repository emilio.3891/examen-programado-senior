﻿using System;
using System.Windows.Forms;
using Controlador;
using Datos;
using Nomina_Rinku.Herramientas;

namespace Nomina_Rinku.Formularios
{
    public partial class FrmBuscarEmpleados : Form
    {
        public FrmBuscarEmpleados()
        {
            InitializeComponent();
        }

        public cat_empleados Empleado { get; set; }

        private void FrmBuscarEmpleados_Load(object sender, EventArgs e)
        {
            cmbTipoBusqueda.SelectedIndex = 0;
            BuscarTodos();
        }

        private void BuscarTodos()
        {
            Buscar(new cat_empleados { num_empleado = null, nom_empleado = string.Empty });
        }

        private void Buscar()
        {
            var emp = new cat_empleados { num_empleado = null, nom_empleado = null };
            if (cmbTipoBusqueda.SelectedIndex == 0)
            {
                int num;
                if (!int.TryParse(txtBuscar.Text, out num))
                {
                    emp.num_empleado = 0;
                    txtBuscar.Text = emp.num_empleado.ToString();
                }
                else
                {
                    emp.num_empleado = num;
                }
            }
            else
            {
                emp.nom_empleado = txtBuscar.Text;
            }

            Buscar(emp);
        }

        private void Buscar(cat_empleados emp)
        {
            try
            {
                var altasEmpleados = new AltasEmpleados();
                var list = altasEmpleados.Proc_buscarEmpleado(emp);
                dgvBuscar.AutoGenerateColumns = false;
                dgvBuscar.Columns[0].DataPropertyName = "num_empleado";
                dgvBuscar.Columns[1].DataPropertyName = "nom_empleado";
                dgvBuscar.DataSource = list;
            }
            catch (Exception e)
            {
                Log.Add(e.Message);
                MensajesEmergentes.MensajeError();
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtBuscar.TextLength > 3) Buscar();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Buscar();
        }

        private void cmbTipoBusqueda_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtBuscar.Text = string.Empty;
            BuscarTodos();
        }

        private void dgvBuscar_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!(sender is DataGridView)) return;
            var dgv = sender as DataGridView;

            if (dgv.CurrentRow == null || !dgv.CurrentRow.Selected) return;

            var altasEmpleados = new AltasEmpleados();
            if (e.RowIndex > -1)
            {
                var list = altasEmpleados.Proc_buscarEmpleado(
                    new cat_empleados
                    {
                        num_empleado = Convert.ToInt32(dgv[0, e.RowIndex].Value)
                    });
                Empleado = list[0];
                Close();
            }
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (cmbTipoBusqueda.SelectedIndex == 0){
                e.Handled = ((!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar) && !Char.IsWhiteSpace(e.KeyChar))) || Char.IsWhiteSpace(e.KeyChar);
            }
            else
            {
                e.Handled = (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar) && !Char.IsWhiteSpace(e.KeyChar));
            }

            if (e.KeyChar == (char)Keys.Enter)
            {
                Buscar();
            }
        }
    }
}