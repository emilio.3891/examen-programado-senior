﻿namespace Nomina_Rinku.Formularios
{
    partial class FrmCorteMensual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCorteMensual = new System.Windows.Forms.Button();
            this.lblDescripcion1 = new System.Windows.Forms.Label();
            this.lblDescripcion2 = new System.Windows.Forms.Label();
            this.lblDescripcion3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCorteMensual
            // 
            this.btnCorteMensual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCorteMensual.Location = new System.Drawing.Point(12, 280);
            this.btnCorteMensual.Name = "btnCorteMensual";
            this.btnCorteMensual.Size = new System.Drawing.Size(860, 41);
            this.btnCorteMensual.TabIndex = 0;
            this.btnCorteMensual.Text = "Corte Mensual";
            this.btnCorteMensual.UseVisualStyleBackColor = true;
            this.btnCorteMensual.Click += new System.EventHandler(this.btnCorteMensual_Click);
            // 
            // lblDescripcion1
            // 
            this.lblDescripcion1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDescripcion1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescripcion1.Location = new System.Drawing.Point(7, 24);
            this.lblDescripcion1.Name = "lblDescripcion1";
            this.lblDescripcion1.Size = new System.Drawing.Size(865, 25);
            this.lblDescripcion1.TabIndex = 1;
            this.lblDescripcion1.Text = "label1";
            this.lblDescripcion1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDescripcion2
            // 
            this.lblDescripcion2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDescripcion2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescripcion2.Location = new System.Drawing.Point(7, 49);
            this.lblDescripcion2.Name = "lblDescripcion2";
            this.lblDescripcion2.Size = new System.Drawing.Size(865, 25);
            this.lblDescripcion2.TabIndex = 3;
            this.lblDescripcion2.Text = "label2";
            this.lblDescripcion2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDescripcion3
            // 
            this.lblDescripcion3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDescripcion3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescripcion3.Location = new System.Drawing.Point(7, 77);
            this.lblDescripcion3.Name = "lblDescripcion3";
            this.lblDescripcion3.Size = new System.Drawing.Size(865, 25);
            this.lblDescripcion3.TabIndex = 5;
            this.lblDescripcion3.Text = "label3";
            this.lblDescripcion3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmCorteMensual
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 611);
            this.Controls.Add(this.lblDescripcion3);
            this.Controls.Add(this.lblDescripcion2);
            this.Controls.Add(this.lblDescripcion1);
            this.Controls.Add(this.btnCorteMensual);
            this.Name = "FrmCorteMensual";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Corte Mensual";
            this.Load += new System.EventHandler(this.FrmCorteMensual_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCorteMensual;
        private System.Windows.Forms.Label lblDescripcion1;
        private System.Windows.Forms.Label lblDescripcion2;
        private System.Windows.Forms.Label lblDescripcion3;
    }
}