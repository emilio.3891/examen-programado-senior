﻿namespace Nomina_Rinku.Formularios
{
    partial class FrmReportesGenereales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConsultar = new System.Windows.Forms.Button();
            this.dpInicio = new System.Windows.Forms.DateTimePicker();
            this.dgvMovimientos = new System.Windows.Forms.DataGridView();
            this.num_empleado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nom_empleado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num_mes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num_anio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fec_corte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imp_subTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imp_total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.des_estatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dpFinal = new System.Windows.Forms.DateTimePicker();
            this.btnXml = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovimientos)).BeginInit();
            this.SuspendLayout();
            // 
            // btnConsultar
            // 
            this.btnConsultar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConsultar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar.Location = new System.Drawing.Point(697, 12);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(86, 31);
            this.btnConsultar.TabIndex = 2;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // dpInicio
            // 
            this.dpInicio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dpInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpInicio.Location = new System.Drawing.Point(12, 12);
            this.dpInicio.Name = "dpInicio";
            this.dpInicio.Size = new System.Drawing.Size(200, 31);
            this.dpInicio.TabIndex = 0;
            this.dpInicio.ValueChanged += new System.EventHandler(this.dpInicio_ValueChanged);
            // 
            // dgvMovimientos
            // 
            this.dgvMovimientos.AllowUserToAddRows = false;
            this.dgvMovimientos.AllowUserToDeleteRows = false;
            this.dgvMovimientos.AllowUserToResizeColumns = false;
            this.dgvMovimientos.AllowUserToResizeRows = false;
            this.dgvMovimientos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMovimientos.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgvMovimientos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMovimientos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.num_empleado,
            this.nom_empleado,
            this.num_mes,
            this.num_anio,
            this.fec_corte,
            this.imp_subTotal,
            this.imp_total,
            this.des_estatus});
            this.dgvMovimientos.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvMovimientos.GridColor = System.Drawing.SystemColors.Control;
            this.dgvMovimientos.Location = new System.Drawing.Point(0, 49);
            this.dgvMovimientos.MultiSelect = false;
            this.dgvMovimientos.Name = "dgvMovimientos";
            this.dgvMovimientos.ReadOnly = true;
            this.dgvMovimientos.RowHeadersVisible = false;
            this.dgvMovimientos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvMovimientos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMovimientos.Size = new System.Drawing.Size(884, 562);
            this.dgvMovimientos.TabIndex = 4;
            // 
            // num_empleado
            // 
            this.num_empleado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.num_empleado.FillWeight = 8F;
            this.num_empleado.HeaderText = "N°";
            this.num_empleado.Name = "num_empleado";
            this.num_empleado.ReadOnly = true;
            // 
            // nom_empleado
            // 
            this.nom_empleado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nom_empleado.FillWeight = 20F;
            this.nom_empleado.HeaderText = "Nombre";
            this.nom_empleado.Name = "nom_empleado";
            this.nom_empleado.ReadOnly = true;
            // 
            // num_mes
            // 
            this.num_mes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.num_mes.FillWeight = 8F;
            this.num_mes.HeaderText = "Mes";
            this.num_mes.Name = "num_mes";
            this.num_mes.ReadOnly = true;
            // 
            // num_anio
            // 
            this.num_anio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.num_anio.FillWeight = 8F;
            this.num_anio.HeaderText = "Año";
            this.num_anio.Name = "num_anio";
            this.num_anio.ReadOnly = true;
            // 
            // fec_corte
            // 
            this.fec_corte.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.fec_corte.FillWeight = 8F;
            this.fec_corte.HeaderText = "Fecha";
            this.fec_corte.Name = "fec_corte";
            this.fec_corte.ReadOnly = true;
            // 
            // imp_subTotal
            // 
            this.imp_subTotal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.imp_subTotal.FillWeight = 10F;
            this.imp_subTotal.HeaderText = "SubTotal";
            this.imp_subTotal.Name = "imp_subTotal";
            this.imp_subTotal.ReadOnly = true;
            // 
            // imp_total
            // 
            this.imp_total.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.imp_total.FillWeight = 10F;
            this.imp_total.HeaderText = "Total";
            this.imp_total.Name = "imp_total";
            this.imp_total.ReadOnly = true;
            // 
            // des_estatus
            // 
            this.des_estatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.des_estatus.FillWeight = 10F;
            this.des_estatus.HeaderText = "Estatus";
            this.des_estatus.Name = "des_estatus";
            this.des_estatus.ReadOnly = true;
            // 
            // dpFinal
            // 
            this.dpFinal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dpFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpFinal.Location = new System.Drawing.Point(218, 12);
            this.dpFinal.Name = "dpFinal";
            this.dpFinal.Size = new System.Drawing.Size(200, 31);
            this.dpFinal.TabIndex = 1;
            this.dpFinal.ValueChanged += new System.EventHandler(this.dpFinal_ValueChanged);
            // 
            // btnXml
            // 
            this.btnXml.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXml.Enabled = false;
            this.btnXml.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnXml.Location = new System.Drawing.Point(789, 12);
            this.btnXml.Name = "btnXml";
            this.btnXml.Size = new System.Drawing.Size(83, 31);
            this.btnXml.TabIndex = 3;
            this.btnXml.Text = "XML";
            this.btnXml.UseVisualStyleBackColor = true;
            this.btnXml.Click += new System.EventHandler(this.btnXml_Click);
            // 
            // FrmReportesGenereales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 611);
            this.Controls.Add(this.btnXml);
            this.Controls.Add(this.dpFinal);
            this.Controls.Add(this.dgvMovimientos);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.dpInicio);
            this.Name = "FrmReportesGenereales";
            this.Text = "FrmReportesGenereales";
            this.Load += new System.EventHandler(this.FrmReportesGenereales_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovimientos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.DateTimePicker dpInicio;
        private System.Windows.Forms.DataGridView dgvMovimientos;
        private System.Windows.Forms.DateTimePicker dpFinal;
        private System.Windows.Forms.Button btnXml;
        private System.Windows.Forms.DataGridViewTextBoxColumn num_empleado;
        private System.Windows.Forms.DataGridViewTextBoxColumn nom_empleado;
        private System.Windows.Forms.DataGridViewTextBoxColumn num_mes;
        private System.Windows.Forms.DataGridViewTextBoxColumn num_anio;
        private System.Windows.Forms.DataGridViewTextBoxColumn fec_corte;
        private System.Windows.Forms.DataGridViewTextBoxColumn imp_subTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn imp_total;
        private System.Windows.Forms.DataGridViewTextBoxColumn des_estatus;
    }
}