﻿namespace Nomina_Rinku.Formularios
{
    partial class FrmConfiguracionesSistema
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.dgvConfiguracionesSistema = new System.Windows.Forms.DataGridView();
            this.txtDescripcionBusqueda = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtActivo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.txtDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConfiguracionesSistema)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Descripción";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(376, 3);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 2;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // dgvConfiguracionesSistema
            // 
            this.dgvConfiguracionesSistema.AllowUserToAddRows = false;
            this.dgvConfiguracionesSistema.AllowUserToDeleteRows = false;
            this.dgvConfiguracionesSistema.AllowUserToResizeColumns = false;
            this.dgvConfiguracionesSistema.AllowUserToResizeRows = false;
            this.dgvConfiguracionesSistema.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvConfiguracionesSistema.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvConfiguracionesSistema.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgvConfiguracionesSistema.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConfiguracionesSistema.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtId,
            this.txtActivo,
            this.txtDescripcion});
            this.dgvConfiguracionesSistema.GridColor = System.Drawing.SystemColors.Control;
            this.dgvConfiguracionesSistema.Location = new System.Drawing.Point(0, 52);
            this.dgvConfiguracionesSistema.MultiSelect = false;
            this.dgvConfiguracionesSistema.Name = "dgvConfiguracionesSistema";
            this.dgvConfiguracionesSistema.RowHeadersVisible = false;
            this.dgvConfiguracionesSistema.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvConfiguracionesSistema.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvConfiguracionesSistema.Size = new System.Drawing.Size(884, 559);
            this.dgvConfiguracionesSistema.TabIndex = 3;
            this.dgvConfiguracionesSistema.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvConfiguracionesSistema_CellValueChanged);
            // 
            // txtDescripcionBusqueda
            // 
            this.txtDescripcionBusqueda.Location = new System.Drawing.Point(81, 5);
            this.txtDescripcionBusqueda.MaxLength = 30;
            this.txtDescripcionBusqueda.Name = "txtDescripcionBusqueda";
            this.txtDescripcionBusqueda.Size = new System.Drawing.Size(208, 20);
            this.txtDescripcionBusqueda.TabIndex = 0;
            this.txtDescripcionBusqueda.TextChanged += new System.EventHandler(this.txtDescripcionBusqueda_TextChanged);
            this.txtDescripcionBusqueda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDescripcionBusqueda_KeyPress);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(295, 3);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 1;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtId
            // 
            this.txtId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtId.FillWeight = 10F;
            this.txtId.HeaderText = "Clave";
            this.txtId.MaxInputLength = 5;
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            // 
            // txtActivo
            // 
            this.txtActivo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtActivo.FillWeight = 10F;
            this.txtActivo.HeaderText = "Activo";
            this.txtActivo.Name = "txtActivo";
            this.txtActivo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.txtActivo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtDescripcion.FillWeight = 80F;
            this.txtDescripcion.HeaderText = "Descripción";
            this.txtDescripcion.MaxInputLength = 50;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.ReadOnly = true;
            // 
            // FrmConfiguracionesSistema
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 611);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.dgvConfiguracionesSistema);
            this.Controls.Add(this.txtDescripcionBusqueda);
            this.Controls.Add(this.btnBuscar);
            this.Name = "FrmConfiguracionesSistema";
            this.Text = "FrmConfiguracionesSistema";
            this.Load += new System.EventHandler(this.FrmConfiguracionesSistema_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvConfiguracionesSistema)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.DataGridView dgvConfiguracionesSistema;
        private System.Windows.Forms.TextBox txtDescripcionBusqueda;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtId;
        private System.Windows.Forms.DataGridViewCheckBoxColumn txtActivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtDescripcion;
    }
}