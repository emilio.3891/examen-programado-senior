﻿namespace Nomina_Rinku.Formularios
{
    partial class FrmMovimientosMasivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvMovimientos = new System.Windows.Forms.DataGridView();
            this.num_empleado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nom_empleado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.des_rol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hrsTrabajadas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hrsExtras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numEntregas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkCubrio = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cmbTurno = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dpFecha = new System.Windows.Forms.DateTimePicker();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.panel = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovimientos)).BeginInit();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvMovimientos
            // 
            this.dgvMovimientos.AllowUserToAddRows = false;
            this.dgvMovimientos.AllowUserToDeleteRows = false;
            this.dgvMovimientos.AllowUserToResizeColumns = false;
            this.dgvMovimientos.AllowUserToResizeRows = false;
            this.dgvMovimientos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMovimientos.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgvMovimientos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMovimientos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.num_empleado,
            this.nom_empleado,
            this.des_rol,
            this.hrsTrabajadas,
            this.hrsExtras,
            this.numEntregas,
            this.chkCubrio,
            this.cmbTurno});
            this.dgvMovimientos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMovimientos.GridColor = System.Drawing.SystemColors.Control;
            this.dgvMovimientos.Location = new System.Drawing.Point(0, 58);
            this.dgvMovimientos.MultiSelect = false;
            this.dgvMovimientos.Name = "dgvMovimientos";
            this.dgvMovimientos.RowHeadersVisible = false;
            this.dgvMovimientos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvMovimientos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMovimientos.Size = new System.Drawing.Size(884, 500);
            this.dgvMovimientos.TabIndex = 2;
            this.dgvMovimientos.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMovimientos_CellValueChanged);
            this.dgvMovimientos.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvMovimientos_DataError);
            // 
            // num_empleado
            // 
            this.num_empleado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.num_empleado.FillWeight = 8F;
            this.num_empleado.HeaderText = "N°";
            this.num_empleado.Name = "num_empleado";
            this.num_empleado.ReadOnly = true;
            // 
            // nom_empleado
            // 
            this.nom_empleado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nom_empleado.FillWeight = 20F;
            this.nom_empleado.HeaderText = "Nombre";
            this.nom_empleado.Name = "nom_empleado";
            this.nom_empleado.ReadOnly = true;
            // 
            // des_rol
            // 
            this.des_rol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.des_rol.FillWeight = 8F;
            this.des_rol.HeaderText = "Rol";
            this.des_rol.Name = "des_rol";
            this.des_rol.ReadOnly = true;
            // 
            // hrsTrabajadas
            // 
            this.hrsTrabajadas.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.hrsTrabajadas.FillWeight = 8F;
            this.hrsTrabajadas.HeaderText = "Hora";
            this.hrsTrabajadas.Name = "hrsTrabajadas";
            // 
            // hrsExtras
            // 
            this.hrsExtras.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.hrsExtras.FillWeight = 8F;
            this.hrsExtras.HeaderText = "Hora Extra";
            this.hrsExtras.Name = "hrsExtras";
            this.hrsExtras.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // numEntregas
            // 
            this.numEntregas.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.numEntregas.FillWeight = 8F;
            this.numEntregas.HeaderText = "Entrega";
            this.numEntregas.Name = "numEntregas";
            // 
            // chkCubrio
            // 
            this.chkCubrio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.chkCubrio.FillWeight = 8F;
            this.chkCubrio.HeaderText = "Cubrió";
            this.chkCubrio.Name = "chkCubrio";
            this.chkCubrio.ReadOnly = true;
            // 
            // cmbTurno
            // 
            this.cmbTurno.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cmbTurno.FillWeight = 8F;
            this.cmbTurno.HeaderText = "Turno";
            this.cmbTurno.Name = "cmbTurno";
            this.cmbTurno.ReadOnly = true;
            this.cmbTurno.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dpFecha
            // 
            this.dpFecha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dpFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpFecha.Location = new System.Drawing.Point(12, 12);
            this.dpFecha.Name = "dpFecha";
            this.dpFecha.Size = new System.Drawing.Size(200, 31);
            this.dpFecha.TabIndex = 0;
            this.dpFecha.ValueChanged += new System.EventHandler(this.dpFecha_ValueChanged);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.Enabled = false;
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Location = new System.Drawing.Point(786, 12);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(86, 31);
            this.btnGuardar.TabIndex = 1;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // panel
            // 
            this.panel.Controls.Add(this.dpFecha);
            this.panel.Controls.Add(this.btnGuardar);
            this.panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(884, 58);
            this.panel.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 558);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(884, 53);
            this.label2.TabIndex = 13;
            this.label2.Text = "Consideraciones:\r\n*Jornada laboral máximo 8 hrs\r\n*Total de horas máximo 24 hrs";
            // 
            // FrmMovimientosMasivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 611);
            this.Controls.Add(this.dgvMovimientos);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.label2);
            this.Name = "FrmMovimientosMasivo";
            this.Text = "FrmMovimientosMasivo";
            this.Load += new System.EventHandler(this.FrmMovimientosMasivo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovimientos)).EndInit();
            this.panel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvMovimientos;
        private System.Windows.Forms.DateTimePicker dpFecha;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.DataGridViewTextBoxColumn num_empleado;
        private System.Windows.Forms.DataGridViewTextBoxColumn nom_empleado;
        private System.Windows.Forms.DataGridViewTextBoxColumn des_rol;
        private System.Windows.Forms.DataGridViewTextBoxColumn hrsTrabajadas;
        private System.Windows.Forms.DataGridViewTextBoxColumn hrsExtras;
        private System.Windows.Forms.DataGridViewTextBoxColumn numEntregas;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkCubrio;
        private System.Windows.Forms.DataGridViewComboBoxColumn cmbTurno;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label label2;
    }
}