﻿namespace Nomina_Rinku.Formularios
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMenu));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.btnAltasEmpleados = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCofMonetarias = new System.Windows.Forms.ToolStripMenuItem();
            this.btnConfSistema = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCorteMensual = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMovimientoIndividual = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMovimientosMasivos = new System.Windows.Forms.ToolStripMenuItem();
            this.btnReportes = new System.Windows.Forms.ToolStripMenuItem();
            this.panel = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAltasEmpleados,
            this.toolStripMenuItem2,
            this.btnCorteMensual,
            this.toolStripMenuItem3,
            this.btnReportes});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(900, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // btnAltasEmpleados
            // 
            this.btnAltasEmpleados.Name = "btnAltasEmpleados";
            this.btnAltasEmpleados.Size = new System.Drawing.Size(157, 20);
            this.btnAltasEmpleados.Text = "Mantenimiento Empleado";
            this.btnAltasEmpleados.Click += new System.EventHandler(this.btnAltasEmpleados_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCofMonetarias,
            this.btnConfSistema});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(106, 20);
            this.toolStripMenuItem2.Text = "Configuraciones";
            // 
            // btnCofMonetarias
            // 
            this.btnCofMonetarias.Name = "btnCofMonetarias";
            this.btnCofMonetarias.Size = new System.Drawing.Size(152, 22);
            this.btnCofMonetarias.Text = "Monetarias";
            this.btnCofMonetarias.Click += new System.EventHandler(this.btnCofMonetarias_Click);
            // 
            // btnConfSistema
            // 
            this.btnConfSistema.Name = "btnConfSistema";
            this.btnConfSistema.Size = new System.Drawing.Size(152, 22);
            this.btnConfSistema.Text = "Sistema";
            this.btnConfSistema.Click += new System.EventHandler(this.btnConfSistema_Click);
            // 
            // btnCorteMensual
            // 
            this.btnCorteMensual.Name = "btnCorteMensual";
            this.btnCorteMensual.Size = new System.Drawing.Size(96, 20);
            this.btnCorteMensual.Text = "Corte Mensual";
            this.btnCorteMensual.Click += new System.EventHandler(this.btnCorteMensual_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnMovimientoIndividual,
            this.btnMovimientosMasivos});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(89, 20);
            this.toolStripMenuItem3.Text = "Movimientos";
            // 
            // btnMovimientoIndividual
            // 
            this.btnMovimientoIndividual.Name = "btnMovimientoIndividual";
            this.btnMovimientoIndividual.Size = new System.Drawing.Size(152, 22);
            this.btnMovimientoIndividual.Text = "Individual";
            this.btnMovimientoIndividual.Click += new System.EventHandler(this.btnMovimientoIndividual_Click);
            // 
            // btnMovimientosMasivos
            // 
            this.btnMovimientosMasivos.Name = "btnMovimientosMasivos";
            this.btnMovimientosMasivos.Size = new System.Drawing.Size(152, 22);
            this.btnMovimientosMasivos.Text = "Masivo";
            this.btnMovimientosMasivos.Click += new System.EventHandler(this.btnMovimientosMasivos_Click);
            // 
            // btnReportes
            // 
            this.btnReportes.Name = "btnReportes";
            this.btnReportes.Size = new System.Drawing.Size(65, 20);
            this.btnReportes.Text = "Reportes";
            this.btnReportes.Click += new System.EventHandler(this.btnReportes_Click);
            // 
            // panel
            // 
            this.panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel.Location = new System.Drawing.Point(0, 24);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(900, 650);
            this.panel.TabIndex = 1;
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(900, 674);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Corporación Rinku";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMenu_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btnAltasEmpleados;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem btnCofMonetarias;
        private System.Windows.Forms.ToolStripMenuItem btnConfSistema;
        private System.Windows.Forms.ToolStripMenuItem btnCorteMensual;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem btnMovimientoIndividual;
        private System.Windows.Forms.ToolStripMenuItem btnMovimientosMasivos;
        private System.Windows.Forms.ToolStripMenuItem btnReportes;
        private System.Windows.Forms.Panel panel;
    }
}