﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;
using Controlador;
using Datos;
using Nomina_Rinku.Herramientas;

namespace Nomina_Rinku.Formularios
{
    public partial class FrmMovimientosMasivo : Form
    {
        /// <summary>
        ///     maneja las consultas al servidor relacionados a los movimientos
        /// </summary>
        private readonly Movimientos _movimientos;

        /// <summary>
        ///     Variable para verificar el estado de carga del dgv|
        /// </summary>
        private bool _cargado;

        /// <summary>
        ///     DataSource asignado al dgv
        /// </summary>
        private List<CapturaMovimientos> _list;

        public FrmMovimientosMasivo()
        {
            InitializeComponent();
            _cargado = false;
            _movimientos = new Movimientos();
        }

        private void dpFecha_ValueChanged(object sender, EventArgs e)
        {
            LlenarDgv();
            btnGuardar.Enabled = true;
        }

        /// <summary>
        ///     Se encarga de llenar la informacion del dgv enlazandolo
        ///     con la lista de recurso y asignar valor no directos
        /// </summary>
        private void LlenarDgv()
        {
            _cargado = false;
            _list = _movimientos.Proc_ConsultarMovimentos(dpFecha.Value);
            cmbTurno.DataSource = _movimientos.Proc_consultarTurnos();

            cmbTurno.ValueMember = "idu_turno";
            cmbTurno.DisplayMember = "des_turno";

            dgvMovimientos.AutoGenerateColumns = false;
            dgvMovimientos.Columns["num_empleado"].DataPropertyName = "num_empleado";
            dgvMovimientos.Columns["nom_empleado"].DataPropertyName = "nom_empleado";
            dgvMovimientos.Columns["des_rol"].DataPropertyName = "des_rol";
            dgvMovimientos.Columns["hrsTrabajadas"].DataPropertyName = "hrsTrabajadas";
            dgvMovimientos.Columns["cmbTurno"].DataPropertyName = "idu_turno";
            dgvMovimientos.Columns["numEntregas"].DataPropertyName = "numEntregas";
            dgvMovimientos.Columns["hrsExtras"].DataPropertyName = "hrsExtras";

            dgvMovimientos.DataSource = _list;

            for (var i = 0; i < dgvMovimientos.Rows.Count; i++)
            {
                var cmb = dgvMovimientos["cmbTurno", i] as DataGridViewComboBoxCell;
                dgvMovimientos["chkCubrio", i].Value = _list[i].idu_turno != 0;
                cmb.Value = _list[i].idu_turno;
                dgvMovimientos["chkCubrio", i].ReadOnly = _list[i].idu_rol != 3;
                dgvMovimientos["cmbTurno", i].ReadOnly = false;
            }

            var conf = new ConfiguracionesSistemas();
            var listConf = conf.Proc_buscarConfigSistema(
                new cat_confSistemas
                {
                    des_confSistema = string.Empty
                });

            dgvMovimientos.Columns["hrsExtras"].Visible = listConf[0].opc_activo;

            _cargado = true;
        }

        private void FrmMovimientosMasivo_Load(object sender, EventArgs e)
        {
            //LlenarDgv();
            ConfigurarFechaMinMax();
        }

        /// <summary>
        ///     Configura la fecha minima y maxima
        /// </summary>
        private void ConfigurarFechaMinMax()
        {
            var fechaCorte = DateTime.Today.Day < 21
                ? new DateTime(DateTime.Today.Year, DateTime.Today.Month, 20)
                : new DateTime(DateTime.Today.Year, DateTime.Today.Month + 1, 20);
            dpFecha.MinDate = fechaCorte.AddMonths(-1).AddDays(1);
            dpFecha.MaxDate = fechaCorte;
        }

        private void dgvMovimientos_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1 || !_cargado) return;
            CambioFila(e.RowIndex);
        }

        /// <summary>
        ///     Se encarga de las validaciones para cuando se cambian las filas del grid
        /// </summary>
        /// <param name="numFila">Indice del grid</param>
        private void CambioFila(int numFila)
        {
            var row = dgvMovimientos.Rows[numFila];

            if (!Convert.ToBoolean(row.Cells["chkCubrio"].Value))
            {
                row.Cells["cmbTurno"].Value = 0;
                row.Cells["cmbTurno"].ReadOnly = true;
            }
            else
            {
                row.Cells["cmbTurno"].ReadOnly = false;
            }

            _list[numFila].modificado = true;
        }

        /// <summary>
        ///     Manejo de errores al asignar datos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvMovimientos_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            var view = (DataGridView)sender;
            e.Cancel = true;
            MensajesEmergentes.MensajeAdvertencia();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (MensajesEmergentes.DeseaGuardar())
            {
                if (ValidaDatosDGV())
                {
                    try
                    {
                        foreach (var item in _list)
                            if (item.modificado)
                                _movimientos.Proc_guardarMovimientos(item);
                        MensajesEmergentes.MensajeActualizado();
                    }
                    catch (Exception exception)
                    {
                        Log.Add(exception.Message);
                        MensajesEmergentes.MensajeNoActualizado();
                    }
                }
                else
                {
                    MensajesEmergentes.ExportadoDatosInvalidos();
                }
            }
        }


        private bool ValidaDatosDGV()
        {
            bool valido = true;
            for (int i = 0; i < _list.Count; i++)
            {
                var mov = _list[i];
                if (mov.hrsTrabajadas + mov.hrsExtras > 24)
                {
                    dgvMovimientos.Rows[i].DefaultCellStyle.BackColor = Color.Pink;
                    valido = false;
                }
                else
                {
                    dgvMovimientos.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(125, 218, 76);
                }
            }

            return valido;
        }
    }
}