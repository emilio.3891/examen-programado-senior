﻿using System;
using System.Linq;
using System.Windows.Forms;
using Controlador;
using Datos;
using Nomina_Rinku.Herramientas;

namespace Nomina_Rinku.Formularios
{
    public partial class FrmMovimientos : Form
    {
        private cat_empleados _emp;

        public FrmMovimientos()
        {
            InitializeComponent();
        }

        private void FrmMovimientos_Load(object sender, EventArgs e)
        {
            ConfigurarFechaMinMax();
            var conf = new ConfiguracionesSistemas();
            var list = conf.Proc_buscarConfigSistema(new cat_confSistemas
            {
                idu_confSistemas = 1
            });
            gbHorasExtras.Visible = list[0].opc_activo;

            var mov = new Movimientos();
            cmbTurnos.DataSource = mov.Proc_consultarTurnos();
            cmbTurnos.ValueMember = "idu_turno";
            cmbTurnos.DisplayMember = "des_turno";
        }


        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var movimientos = new Movimientos();
            var frmBuscarEmpleados = new FrmBuscarEmpleados();
            frmBuscarEmpleados.ShowDialog();
            if (frmBuscarEmpleados.Empleado != null)
            {
                _emp = movimientos.Proc_buscarEmpleadoDetalle(frmBuscarEmpleados.Empleado);
                EstablecerDatos(_emp);
            }
        }

        private void EstablecerDatos(cat_empleados empleado)
        {
            if (empleado == null)
            {
                txtNumero.Text = string.Empty;
                txtNombre.Text = string.Empty;
                txtRol.Text = string.Empty;
                txtTipo.Text = string.Empty;
                gbCubrio.Visible = false;
            }
            else
            {
                txtNumero.Text = empleado.num_empleado.ToString() ?? string.Empty;
                txtNombre.Text = empleado.nom_empleado ?? string.Empty;
                txtRol.Text = empleado.CatEmpleadosDetalles.des_rol ?? string.Empty;
                txtTipo.Text = empleado.CatEmpleadosDetalles.des_tipoEmpleado ?? string.Empty;
                gbCubrio.Visible = empleado.CatEmpleadosDetalles.idu_rol == 3;
                BuscarMovimiento();
            }
        }

        private void chkCubrioTurno_CheckedChanged(object sender, EventArgs e)
        {
            cmbTurnos.Visible = chkCubrioTurno.Checked;
            lblTurno.Visible = chkCubrioTurno.Checked;
        }

        private void ResetearControles()
        {
            _emp = null;
            EstablecerDatos(_emp);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (_emp != null)
            {
                if (MensajesEmergentes.DeseaGuardar())
                {
                    var capturaMov = new CapturaMovimientos
                    {
                        num_empleado = Convert.ToInt32(txtNumero.Text),
                        fec_movimiento = dtFecha.Value,
                        hrsTrabajadas = Convert.ToInt32(txtHoras.Value),
                        hrsExtras = Convert.ToInt32(txtHorasExtra.Value),
                        numEntregas = Convert.ToInt32(txtEntregas.Value),
                        idu_turno = 0
                    };
                    if (_emp.CatEmpleadosDetalles.idu_rol == 3 && chkCubrioTurno.Checked)
                        capturaMov.idu_turno = Convert.ToInt32(cmbTurnos.SelectedValue);

                    try
                    {
                        var _movimientos = new Movimientos();
                        _movimientos.Proc_guardarMovimientos(capturaMov);

                        MensajesEmergentes.MensajeActualizado();
                        btnBuscar.Focus();
                    }
                    catch (Exception exception)
                    {
                        Log.Add(exception.Message);
                        MensajesEmergentes.MensajeNoActualizado();
                    }

                    ResetearControles();
                }
                else
                {
                    MensajesEmergentes.MensajeAdvertencia("Favor de seleccionar un empleado");
                }
            }
        }

        private void dtFecha_ValueChanged(object sender, EventArgs e)
        {
            BuscarMovimiento();
        }

        private void BuscarMovimiento()
        {
            if (txtNumero.TextLength > 0)
            {
                var movimientos = new Movimientos();
                var list = movimientos.Proc_ConsultarMovimentos(dtFecha.Value);
                var emp = list.First(m =>
                    m.num_empleado == Convert.ToInt32(txtNumero.Text));

                txtHoras.Value = emp.hrsTrabajadas;
                txtHorasExtra.Value = emp.hrsExtras;
                txtEntregas.Value = emp.numEntregas;
                chkCubrioTurno.Checked = emp.idu_turno != 0;
                cmbTurnos.SelectedValue = emp.idu_turno;
            }
        }

        /// <summary>
        ///     Configura la fecha minima y maxima
        /// </summary>
        private void ConfigurarFechaMinMax()
        {
            var fechaCorte = DateTime.Today.Day < 21
                ? new DateTime(DateTime.Today.Year, DateTime.Today.Month, 20)
                : new DateTime(DateTime.Today.Year, DateTime.Today.Month + 1, 20);
            dtFecha.MinDate = fechaCorte.AddMonths(-1).AddDays(1);
            dtFecha.MaxDate = fechaCorte;
        }

        private void txtHoras_ValueChanged(object sender, EventArgs e)
        {
            txtHorasExtra.Maximum = 24 - txtHoras.Value;
        }
    }
}