﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Controlador;
using Datos;
using Nomina_Rinku.Herramientas;
using System.Linq;
namespace Nomina_Rinku.Formularios
{
    public partial class FrmConfiguracionesSistema : Form
    {
        private readonly ConfiguracionesSistemas _configuracionesSistemas;
        private List<cat_confSistemas> _listConf;

        public FrmConfiguracionesSistema()
        {
            InitializeComponent();
            _configuracionesSistemas = new ConfiguracionesSistemas();
        }

        private void BuscarConfSistema(cat_confSistemas catconfSistemas)
        {
            _listConf = _configuracionesSistemas.Proc_buscarConfigSistema(catconfSistemas);

            dgvConfiguracionesSistema.AutoGenerateColumns = false;
            dgvConfiguracionesSistema.Columns[0].DataPropertyName = "idu_confSistemas";
            dgvConfiguracionesSistema.Columns[1].DataPropertyName = "opc_activo";
            dgvConfiguracionesSistema.Columns[2].DataPropertyName = "des_confSistema";
            dgvConfiguracionesSistema.DataSource = _listConf;
        }

        private void FrmConfiguracionesSistema_Load(object sender, EventArgs e)
        {
            Buscar();
        }

        private void txtDescripcionBusqueda_TextChanged(object sender, EventArgs e)
        {
            if (txtDescripcionBusqueda.TextLength > 3 || txtDescripcionBusqueda.TextLength == 0) Buscar();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Buscar();
        }

        private void Buscar()
        {
            try
            {
                var catconfSistemas = new cat_confSistemas
                {
                    idu_confSistemas = null,
                    des_confSistema = txtDescripcionBusqueda.Text
                };

                BuscarConfSistema(catconfSistemas);
            }
            catch (Exception exception)
            {
                Log.Add(exception.Message);
                MensajesEmergentes.MensajeError();
            }
        }

        private void Guardar(cat_confSistemas conf)
        {
            _configuracionesSistemas.Proc_GuardarConfigSistema(conf);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (MensajesEmergentes.DeseaGuardar() && MensajesEmergentes.CerrarSistemaParaCambios())
            {
                try
                {
                    foreach (var catConfMonetarias in _listConf)
                    {
                        if (catConfMonetarias.modificado)
                        {
                            _configuracionesSistemas.Proc_GuardarConfigSistema(catConfMonetarias);
                        }
                    }
                    MensajesEmergentes.MensajeActualizado();


                    var form = Application.OpenForms.OfType<FrmMenu>().FirstOrDefault();
                    form.FinalizarAplicacion();
                    
                }
                catch (Exception exception)
                {
                    Log.Add(exception.Message);
                    MensajesEmergentes.MensajeNoActualizado();
                }
            }
        }

        private void dgvConfiguracionesSistema_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (_listConf != null) _listConf[e.RowIndex].modificado = true;
        }

        private void txtDescripcionBusqueda_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar) && !Char.IsWhiteSpace(e.KeyChar));
        }
    }
}