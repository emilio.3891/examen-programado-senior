﻿using System;
using System.Windows.Forms;
using Controlador;
using Nomina_Rinku.Herramientas;

namespace Nomina_Rinku.Formularios
{
    public partial class FrmCorteMensual : Form
    {
        public FrmCorteMensual()
        {
            InitializeComponent();
        }

        private string TextoDescripcion1 { get { return "Se realizará el corte mensual"; } }
        private string TextoDescripcion2 { get { return "de todos los empleados"; } }
        private string TextoDescripcion3 { get { return "fecha corte 21/{0}."; } }

        private void FrmCorteMensual_Load(object sender, EventArgs e)
        {
            lblDescripcion1.Text = TextoDescripcion1;
            lblDescripcion2.Text = TextoDescripcion2;
            lblDescripcion3.Text = string.Format(TextoDescripcion3, DateTime.Now.ToString("MM/yyyy"));
        }

        private void btnCorteMensual_Click(object sender, EventArgs e)
        {
            if (MensajesEmergentes.DeseaGuardar())
            {
                try
                {
                    var corteMensual = new CorteMensual();
                    var filas = corteMensual.Proc_corteMensual();
                    MensajesEmergentes.MensajeExitoso();
                }
                catch (Exception exception)
                {
                    Log.Add(exception.Message);
                    MensajesEmergentes.MensajeError();
                }
            }
        }
    }
}