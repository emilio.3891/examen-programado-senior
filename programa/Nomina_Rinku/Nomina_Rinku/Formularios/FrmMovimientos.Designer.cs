﻿namespace Nomina_Rinku.Formularios
{
    partial class FrmMovimientos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRol = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTipo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtHoras = new System.Windows.Forms.NumericUpDown();
            this.chkCubrioTurno = new System.Windows.Forms.CheckBox();
            this.gbHorasExtras = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtHorasExtra = new System.Windows.Forms.NumericUpDown();
            this.dtFecha = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbTurnos = new System.Windows.Forms.ComboBox();
            this.lblTurno = new System.Windows.Forms.Label();
            this.txtEntregas = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.gbCubrio = new System.Windows.Forms.GroupBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoras)).BeginInit();
            this.gbHorasExtras.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHorasExtra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEntregas)).BeginInit();
            this.gbCubrio.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "N° Empleado";
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(84, 8);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.ReadOnly = true;
            this.txtNumero.Size = new System.Drawing.Size(131, 20);
            this.txtNumero.TabIndex = 0;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(329, 8);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(224, 20);
            this.txtNombre.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(279, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nombre";
            // 
            // txtRol
            // 
            this.txtRol.Location = new System.Drawing.Point(329, 34);
            this.txtRol.Name = "txtRol";
            this.txtRol.ReadOnly = true;
            this.txtRol.Size = new System.Drawing.Size(224, 20);
            this.txtRol.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(300, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Rol";
            // 
            // txtTipo
            // 
            this.txtTipo.Location = new System.Drawing.Point(84, 34);
            this.txtTipo.Name = "txtTipo";
            this.txtTipo.ReadOnly = true;
            this.txtTipo.Size = new System.Drawing.Size(131, 20);
            this.txtTipo.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(50, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Tipo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Fecha";
            // 
            // txtHoras
            // 
            this.txtHoras.Location = new System.Drawing.Point(84, 89);
            this.txtHoras.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.txtHoras.Name = "txtHoras";
            this.txtHoras.Size = new System.Drawing.Size(131, 20);
            this.txtHoras.TabIndex = 5;
            this.txtHoras.ValueChanged += new System.EventHandler(this.txtHoras_ValueChanged);
            // 
            // chkCubrioTurno
            // 
            this.chkCubrioTurno.AutoSize = true;
            this.chkCubrioTurno.Location = new System.Drawing.Point(9, 26);
            this.chkCubrioTurno.Name = "chkCubrioTurno";
            this.chkCubrioTurno.Size = new System.Drawing.Size(87, 17);
            this.chkCubrioTurno.TabIndex = 11;
            this.chkCubrioTurno.Text = "Cubrio Turno";
            this.chkCubrioTurno.UseVisualStyleBackColor = true;
            this.chkCubrioTurno.CheckedChanged += new System.EventHandler(this.chkCubrioTurno_CheckedChanged);
            // 
            // gbHorasExtras
            // 
            this.gbHorasExtras.Controls.Add(this.label8);
            this.gbHorasExtras.Controls.Add(this.txtHorasExtra);
            this.gbHorasExtras.Location = new System.Drawing.Point(32, 141);
            this.gbHorasExtras.Name = "gbHorasExtras";
            this.gbHorasExtras.Size = new System.Drawing.Size(521, 55);
            this.gbHorasExtras.TabIndex = 8;
            this.gbHorasExtras.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Horas Extras";
            // 
            // txtHorasExtra
            // 
            this.txtHorasExtra.Location = new System.Drawing.Point(77, 17);
            this.txtHorasExtra.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.txtHorasExtra.Name = "txtHorasExtra";
            this.txtHorasExtra.Size = new System.Drawing.Size(95, 20);
            this.txtHorasExtra.TabIndex = 0;
            // 
            // dtFecha
            // 
            this.dtFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFecha.Location = new System.Drawing.Point(84, 63);
            this.dtFecha.Name = "dtFecha";
            this.dtFecha.Size = new System.Drawing.Size(131, 20);
            this.dtFecha.TabIndex = 4;
            this.dtFecha.ValueChanged += new System.EventHandler(this.dtFecha_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(50, 93);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Horas";
            // 
            // cmbTurnos
            // 
            this.cmbTurnos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTurnos.FormattingEnabled = true;
            this.cmbTurnos.Location = new System.Drawing.Point(47, 45);
            this.cmbTurnos.Name = "cmbTurnos";
            this.cmbTurnos.Size = new System.Drawing.Size(188, 21);
            this.cmbTurnos.TabIndex = 0;
            this.cmbTurnos.Visible = false;
            // 
            // lblTurno
            // 
            this.lblTurno.AutoSize = true;
            this.lblTurno.Location = new System.Drawing.Point(6, 48);
            this.lblTurno.Name = "lblTurno";
            this.lblTurno.Size = new System.Drawing.Size(35, 13);
            this.lblTurno.TabIndex = 4;
            this.lblTurno.Text = "Turno";
            this.lblTurno.Visible = false;
            // 
            // txtEntregas
            // 
            this.txtEntregas.Location = new System.Drawing.Point(84, 115);
            this.txtEntregas.Name = "txtEntregas";
            this.txtEntregas.Size = new System.Drawing.Size(131, 20);
            this.txtEntregas.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(29, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Entregas";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(221, 8);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(52, 23);
            this.btnBuscar.TabIndex = 18;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // gbCubrio
            // 
            this.gbCubrio.Controls.Add(this.cmbTurnos);
            this.gbCubrio.Controls.Add(this.lblTurno);
            this.gbCubrio.Controls.Add(this.chkCubrioTurno);
            this.gbCubrio.Location = new System.Drawing.Point(282, 63);
            this.gbCubrio.Name = "gbCubrio";
            this.gbCubrio.Size = new System.Drawing.Size(271, 72);
            this.gbCubrio.TabIndex = 7;
            this.gbCubrio.TabStop = false;
            this.gbCubrio.Visible = false;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(28, 202);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 9;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(0, 557);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(884, 54);
            this.label7.TabIndex = 19;
            this.label7.Text = "Consideraciones:\r\n*Jornada laboral máximo 8 hrs\r\n*Total de horas máximo 24 hrs";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtNumero);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnGuardar);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.gbCubrio);
            this.panel1.Controls.Add(this.txtNombre);
            this.panel1.Controls.Add(this.btnBuscar);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtEntregas);
            this.panel1.Controls.Add(this.txtRol);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtHoras);
            this.panel1.Controls.Add(this.txtTipo);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.dtFecha);
            this.panel1.Controls.Add(this.gbHorasExtras);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(884, 237);
            this.panel1.TabIndex = 20;
            // 
            // FrmMovimientos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 611);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label7);
            this.Name = "FrmMovimientos";
            this.Text = "FrmMovimientos";
            this.Load += new System.EventHandler(this.FrmMovimientos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtHoras)).EndInit();
            this.gbHorasExtras.ResumeLayout(false);
            this.gbHorasExtras.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHorasExtra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEntregas)).EndInit();
            this.gbCubrio.ResumeLayout(false);
            this.gbCubrio.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRol;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTipo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown txtHoras;
        private System.Windows.Forms.CheckBox chkCubrioTurno;
        private System.Windows.Forms.GroupBox gbHorasExtras;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown txtHorasExtra;
        private System.Windows.Forms.DateTimePicker dtFecha;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbTurnos;
        private System.Windows.Forms.Label lblTurno;
        private System.Windows.Forms.NumericUpDown txtEntregas;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.GroupBox gbCubrio;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
    }
}