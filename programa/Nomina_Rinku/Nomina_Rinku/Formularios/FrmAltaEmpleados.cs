﻿using System;
using System.Windows.Forms;
using Controlador;
using Datos;
using Nomina_Rinku.Herramientas;

namespace Nomina_Rinku.Formularios
{
    public partial class FrmAltaEmpleados : Form
    {
        private readonly AltasEmpleados _altasEmpleados;

        public FrmAltaEmpleados()
        {
            InitializeComponent();
            _altasEmpleados = new AltasEmpleados();
        }

        private void FrmAltaEmpleados_Load(object sender, EventArgs e)
        {
            ConfiguraPantalla();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Nuevo();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscarEmpleado();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (GuardarEsPosible())
            {
                if (MensajesEmergentes.DeseaGuardar())
                {
                    Guardar();
                    btnBuscar.Focus();
                }
                else
                {
                    MensajesEmergentes.MensajeFavorLlenarCampos();
                }
            }
        }

        /// <summary>
        ///     Se encarga de configurar la pantalla a mostrar estableciendo los valores por defecto
        /// </summary>
        public void ConfiguraPantalla()
        {
            var conf = new ConfiguracionesSistemas();
            var list = conf.Proc_buscarConfigSistema(
                new cat_confSistemas
                {
                    des_confSistema = string.Empty
                });

            chkImss.Visible = list[1].opc_activo;
            chkInfonavit.Visible = list[2].opc_activo;

            gbExtra.Visible = list[1].opc_activo || list[2].opc_activo;
            LlenarComboBoxs();
            Nuevo();
        }

        /// <summary>
        ///     Consulta los datos de los comboBox y los asigna
        /// </summary>
        private void LlenarComboBoxs()
        {
            cmbRol.DataSource = _altasEmpleados.Proc_consultarRoles();
            cmbRol.ValueMember = "idu_rol";
            cmbRol.DisplayMember = "des_rol";
            cmbTipo.DataSource = _altasEmpleados.Proc_consultarTiposEmpleados();
            cmbTipo.ValueMember = "idu_tipoEmpleado";
            cmbTipo.DisplayMember = "des_tipoEmpleado";
        }

        /// <summary>
        ///     Asigna estable la pantalla para permitir registrar un nuevo empleado
        /// </summary>
        private void Nuevo()
        {
            EstablecerDatos(_altasEmpleados.Proc_ConseguirNuevoEmpleado());
        }

        /// <summary>
        ///     Levanta el formulario para busqueda avanzada de un empleado
        /// </summary>
        private void BuscarEmpleado()
        {
            var frmBuscarEmpleados = new FrmBuscarEmpleados();
            frmBuscarEmpleados.ShowDialog();
            if (frmBuscarEmpleados.Empleado != null) EstablecerDatos(frmBuscarEmpleados.Empleado);
        }

        /// <summary>
        ///     Transforma los datos ingresados por el usario a una estructura de datos
        /// </summary>
        /// <returns>Estructura llenada</returns>
        private cat_empleados ConseguirEmpleados()
        {
            var empleado = new cat_empleados
            {
                num_empleado = Convert.ToInt32(txtNum.Text),
                nom_empleado = txtNombre.Text,
                CatEmpleadosDetalles = new cat_empleadosDetalles
                {
                    num_empleado = Convert.ToInt32(txtNum.Text),
                    idu_rol = Convert.ToInt32(cmbRol.SelectedValue),
                    idu_tipoEmpleado = Convert.ToInt32(cmbTipo.SelectedValue),
                    clv_imss = chkImss.Checked,
                    clv_infonavit = chkInfonavit.Checked
                }
            };
            return empleado;
        }

        /// <summary>
        ///     Estable los datos de acuerdo a la estructuctura, llenando los controles
        /// </summary>
        /// <param name="empleado">Estructura que contiene la informacion para llenado de datos</param>
        private void EstablecerDatos(cat_empleados empleado)
        {
            txtNum.Text = empleado.num_empleado.ToString();
            txtNombre.Text = empleado.nom_empleado;
            cmbRol.SelectedValue = empleado.CatEmpleadosDetalles.idu_rol;
            cmbTipo.SelectedValue = empleado.CatEmpleadosDetalles.idu_tipoEmpleado;
            chkImss.Checked = Convert.ToBoolean(Convert.ToInt16(empleado.CatEmpleadosDetalles.clv_imss));
            chkInfonavit.Checked = Convert.ToBoolean(Convert.ToInt16(empleado.CatEmpleadosDetalles.clv_infonavit));
        }

        private bool GuardarEsPosible()
        {
            return !string.IsNullOrWhiteSpace(txtNombre.Text);
        }

        /// <summary>
        ///     Guarda los datos del empleado nuevo o existente
        /// </summary>
        private void Guardar()
        {
            try
            {
                _altasEmpleados.Proc_GuardarEmpleado(ConseguirEmpleados());
                MensajesEmergentes.MensajeGuardado();
                Nuevo();
            }
            catch (Exception e)
            {
                Log.Add(e.Message);
                MensajesEmergentes.MensajeNoGuardado();
            }
        }

        private void txtNum_Click(object sender, EventArgs e)
        {
            BuscarEmpleado();
        }

        private void txtNum_Enter(object sender, EventArgs e)
        {
            BuscarEmpleado();
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar) && !Char.IsWhiteSpace(e.KeyChar));
        }
    }
}