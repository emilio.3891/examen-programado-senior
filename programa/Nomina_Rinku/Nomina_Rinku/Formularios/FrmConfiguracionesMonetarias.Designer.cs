﻿namespace Nomina_Rinku.Formularios
{
    partial class FrmConfiguracionesMonetarias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtDescripcionBusqueda = new System.Windows.Forms.TextBox();
            this.dgvConfiguracionesMonetarias = new System.Windows.Forms.DataGridView();
            this.txtId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtValor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConfiguracionesMonetarias)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(295, 3);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 1;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtDescripcionBusqueda
            // 
            this.txtDescripcionBusqueda.Location = new System.Drawing.Point(81, 5);
            this.txtDescripcionBusqueda.MaxLength = 30;
            this.txtDescripcionBusqueda.Name = "txtDescripcionBusqueda";
            this.txtDescripcionBusqueda.Size = new System.Drawing.Size(208, 20);
            this.txtDescripcionBusqueda.TabIndex = 0;
            this.txtDescripcionBusqueda.TextChanged += new System.EventHandler(this.txtDescripcionBusqueda_TextChanged);
            this.txtDescripcionBusqueda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDescripcionBusqueda_KeyPress);
            // 
            // dgvConfiguracionesMonetarias
            // 
            this.dgvConfiguracionesMonetarias.AllowUserToAddRows = false;
            this.dgvConfiguracionesMonetarias.AllowUserToDeleteRows = false;
            this.dgvConfiguracionesMonetarias.AllowUserToResizeColumns = false;
            this.dgvConfiguracionesMonetarias.AllowUserToResizeRows = false;
            this.dgvConfiguracionesMonetarias.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvConfiguracionesMonetarias.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvConfiguracionesMonetarias.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgvConfiguracionesMonetarias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConfiguracionesMonetarias.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtId,
            this.txtValor,
            this.txtDescripcion});
            this.dgvConfiguracionesMonetarias.GridColor = System.Drawing.SystemColors.Control;
            this.dgvConfiguracionesMonetarias.Location = new System.Drawing.Point(0, 52);
            this.dgvConfiguracionesMonetarias.MultiSelect = false;
            this.dgvConfiguracionesMonetarias.Name = "dgvConfiguracionesMonetarias";
            this.dgvConfiguracionesMonetarias.RowHeadersVisible = false;
            this.dgvConfiguracionesMonetarias.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvConfiguracionesMonetarias.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvConfiguracionesMonetarias.Size = new System.Drawing.Size(884, 559);
            this.dgvConfiguracionesMonetarias.TabIndex = 3;
            this.dgvConfiguracionesMonetarias.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvConfiguracionesMonetarias_CellValueChanged);
            this.dgvConfiguracionesMonetarias.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvConfiguracionesMonetarias_DataError);
            // 
            // txtId
            // 
            this.txtId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtId.FillWeight = 20F;
            this.txtId.HeaderText = "Id";
            this.txtId.MaxInputLength = 5;
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            // 
            // txtValor
            // 
            this.txtValor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtValor.FillWeight = 40F;
            this.txtValor.HeaderText = "Valor";
            this.txtValor.MaxInputLength = 30;
            this.txtValor.Name = "txtValor";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtDescripcion.FillWeight = 40F;
            this.txtDescripcion.HeaderText = "Descripción";
            this.txtDescripcion.MaxInputLength = 50;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.ReadOnly = true;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(376, 3);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 2;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Descripción";
            // 
            // FrmConfiguracionesMonetarias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(884, 611);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.dgvConfiguracionesMonetarias);
            this.Controls.Add(this.txtDescripcionBusqueda);
            this.Controls.Add(this.btnBuscar);
            this.Name = "FrmConfiguracionesMonetarias";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuraciones Monetarias";
            this.Load += new System.EventHandler(this.FrmConfiguracionesMonetarias_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvConfiguracionesMonetarias)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtDescripcionBusqueda;
        private System.Windows.Forms.DataGridView dgvConfiguracionesMonetarias;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtId;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtValor;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtDescripcion;
    }
}