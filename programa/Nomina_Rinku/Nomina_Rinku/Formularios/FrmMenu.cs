﻿using System;
using System.Linq;
using System.Windows.Forms;
using Nomina_Rinku.Herramientas;

namespace Nomina_Rinku.Formularios
{
    public partial class FrmMenu : Form
    {
        public FrmMenu()
        {
            InitializeComponent();
        }

        private bool _salir = false;

        private void AddFormInPanel(Form fh)
        {
            if (panel.Controls.Count > 0) panel.Controls.RemoveAt(0);
            fh.TopLevel = false;
            fh.FormBorderStyle = FormBorderStyle.None;
            fh.Dock = DockStyle.Fill;
            panel.Controls.Add(fh);
            panel.Tag = fh;
            fh.Show();
        }

        private void btnAltasEmpleados_Click(object sender, EventArgs e)
        {
            this.Text = "Mantenimiento Empleado";
            var form = Application.OpenForms.OfType<FrmAltaEmpleados>().FirstOrDefault();
            var hijo = form ?? new FrmAltaEmpleados();
            AddFormInPanel(hijo);
        }

        private void btnCofMonetarias_Click(object sender, EventArgs e)
        {
            this.Text = "Configuraciones Monetarias";
            var form = Application.OpenForms.OfType<FrmConfiguracionesMonetarias>().FirstOrDefault();
            var hijo = form ?? new FrmConfiguracionesMonetarias();
            AddFormInPanel(hijo);
        }

        private void btnConfSistema_Click(object sender, EventArgs e)
        {
            this.Text = "Configuraciones Sistemas";
            var form = Application.OpenForms.OfType<FrmConfiguracionesSistema>().FirstOrDefault();
            var hijo = form ?? new FrmConfiguracionesSistema();
            AddFormInPanel(hijo);
        }

        private void btnCorteMensual_Click(object sender, EventArgs e)
        {
            this.Text = "Corte Mensual";
            var form = Application.OpenForms.OfType<FrmCorteMensual>().FirstOrDefault();
            var hijo = form ?? new FrmCorteMensual();
            AddFormInPanel(hijo);
        }

        private void btnMovimientoIndividual_Click(object sender, EventArgs e)
        {
            this.Text = "Movimientos Individual";
            var form = Application.OpenForms.OfType<FrmMovimientos>().FirstOrDefault();
            var hijo = form ?? new FrmMovimientos();
            AddFormInPanel(hijo);
        }

        private void btnMovimientosMasivos_Click(object sender, EventArgs e)
        {
            this.Text = "Movimientos Masivos";
            var hijo = new FrmMovimientosMasivo();
            AddFormInPanel(hijo);
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            this.Text = "Reporte";
            var form = Application.OpenForms.OfType<FrmReportesGenereales>().FirstOrDefault();
            var hijo = form ?? new FrmReportesGenereales();
            AddFormInPanel(hijo);
        }

        private void FrmMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = (!_salir && !MensajesEmergentes.DeseaSalir());
        }

        public void FinalizarAplicacion()
        {
            _salir = true;
            this.Close();
        }
    }
}