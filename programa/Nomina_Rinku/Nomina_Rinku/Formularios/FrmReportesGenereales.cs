﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Controlador;
using Nomina_Rinku.Herramientas;
namespace Nomina_Rinku.Formularios
{
    public partial class FrmReportesGenereales : Form
    {
        private readonly Reportes _reportes;

        public FrmReportesGenereales()
        {
            InitializeComponent();
            _reportes = new Reportes();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            dgvMovimientos.AutoGenerateColumns = false;
            dgvMovimientos.Columns["num_empleado"].DataPropertyName = "num_empleado";
            dgvMovimientos.Columns["nom_empleado"].DataPropertyName = "nom_empleado";
            dgvMovimientos.Columns["num_mes"].DataPropertyName = "num_mes";
            dgvMovimientos.Columns["num_anio"].DataPropertyName = "num_anio";
            dgvMovimientos.Columns["fec_corte"].DataPropertyName = "fec_corte";
            dgvMovimientos.Columns["imp_subTotal"].DataPropertyName = "imp_subTotal";
            dgvMovimientos.Columns["imp_total"].DataPropertyName = "imp_total";
            dgvMovimientos.Columns["des_estatus"].DataPropertyName = "des_estatus";

            dgvMovimientos.DataSource = _reportes.Proc_ConsultarEstadisticaGeneral(dpInicio.Value, dpFinal.Value);
            btnXml.Enabled = dgvMovimientos.Rows.Count > 0;
        }

        private DataTable GetDataTableFromDGV(DataGridView dgv)
        {
            var dt = new DataTable("ReporteGeneral");
            foreach (DataGridViewColumn column in dgv.Columns)
                if (column.Visible)
                    dt.Columns.Add(column.HeaderText, typeof(string));

            var cellValues = new object[dgv.Columns.Count];
            foreach (DataGridViewRow row in dgv.Rows)
            {
                for (var i = 0; i < row.Cells.Count; i++) cellValues[i] = row.Cells[i].Value;
                dt.Rows.Add(cellValues);
            }

            return dt;
        }

        private void btnXml_Click(object sender, EventArgs e)
        {
            if (dgvMovimientos.Rows.Count > 0)
            {
                ExportarXML();
            }
            else
            {
                MensajesEmergentes.SinDatos();
            }
        }

        private void ExportarXML()
        {
            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "XML|*.xml";
            saveFileDialog.Title = "Guardar como XML";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                var ds = new DataSet();
                ds.Tables.Add(GetDataTableFromDGV(dgvMovimientos));

                var newXml = new XmlTextWriter(saveFileDialog.FileName, Encoding.UTF8);
                ds.WriteXml(newXml);
                MensajesEmergentes.ExportadoExitoso();
            }
        }

        private void dpInicio_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void dpFinal_ValueChanged(object sender, EventArgs e)
        {
            dpInicio.MaxDate = dpFinal.Value;
        }

        private void FrmReportesGenereales_Load(object sender, EventArgs e)
        {
            dpInicio.MinDate = new DateTime(2018, 01, 01);
            dpFinal.MinDate = new DateTime(2018, 01, 01);
            dpFinal.MaxDate = DateTime.Today.AddMonths(1);
            dpInicio.MaxDate = dpFinal.Value;
        }
    }
}