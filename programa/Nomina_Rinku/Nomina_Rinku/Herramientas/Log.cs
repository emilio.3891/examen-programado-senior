﻿using System;
using System.IO;

namespace Nomina_Rinku.Herramientas
{
    public static class Log
    {
        /// <summary>
        /// Ruta del log
        /// </summary>
        private static string Path { get { return "./log"; } }
        /// <summary>
        /// Agrega nuevo registro al log
        /// </summary>
        /// <param name="sLog"></param>
        public static void Add(string sLog)
        {
            CreateDirectory();
            var nombre = GetNameFile();
            var cadena = string.Empty;

            cadena += DateTime.Now + " - " + sLog + Environment.NewLine;

            var sw = new StreamWriter(Path + "/" + nombre, true);
            sw.Write(cadena);
            sw.Close();
        }

        #region HELPER
        /// <summary>
        /// Obtiene el nombre del log
        /// </summary>
        /// <returns></returns>
        private static string GetNameFile()
        {
            var nombre = "";

            nombre = "log_" + DateTime.Now.Year + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + ".txt";

            return nombre;
        }
        /// <summary>
        /// Crea el directorio si no existe
        /// </summary>
        private static void CreateDirectory()
        {
            try
            {
                if (!Directory.Exists(Path)) Directory.CreateDirectory(Path);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion
    }
}