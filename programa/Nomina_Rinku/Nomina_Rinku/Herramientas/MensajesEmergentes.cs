﻿using System.Windows.Forms;
using Mensajes;

namespace Nomina_Rinku.Herramientas
{
    public static class MensajesEmergentes
    {
        private static string Caption { get { return "[NÓMINA RINKU]"; } }

        public static void MensajeExitoso()
        {
            MensajeExitoso(Mensaje.Exito);
        }

        public static void MensajeGuardado()
        {
            MensajeExitoso(Mensaje.Guardado);
        }

        public static void MensajeActualizado()
        {
            MensajeExitoso(Mensaje.Actualizado);
        }

        public static void MensajeExitoso(string msg)
        {
            MessageBox.Show(msg, Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void MensajeError()
        {
            MensajeError(Mensaje.Error);
        }

        public static void MensajeNoGuardado()
        {
            MensajeError(Mensaje.NoGuardado);
        }

        public static void MensajeNoActualizado()
        {
            MensajeError(Mensaje.NoActualizado);
        }

        public static void MensajeError(string msg)
        {
            MessageBox.Show(msg, Caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void MensajeAdvertencia()
        {
            MensajeAdvertencia("Valor inválido");
        }

        public static void MensajeAdvertencia(string msg)
        {
            MessageBox.Show(msg, Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void MensajeFavorLlenarCampos()
        {
            MensajeExitoso(Mensaje.FavorLlenarCampos);
        }

        public static void SinDatos()
        {
            var msg = "No existen datos para exportar";
            MensajeExitoso(msg);
        }

        public static bool DeseaSalir()
        {
            var msg = "¿Realmente desea salir?";
            DialogResult result = MessageBox.Show(msg, Caption, MessageBoxButtons.YesNo);
            return result == DialogResult.Yes;
        }

        public static bool DeseaGuardar()
        {
            var msg = "¿Realmente desea guardar?";
            DialogResult result = MessageBox.Show(msg, Caption, MessageBoxButtons.YesNo);
            return result == DialogResult.Yes;
        }

        public static bool DeseaRealizarCorteMensual()
        {
            var msg = "¿Realmente desea realizar corte mensual?";
            DialogResult result = MessageBox.Show(msg, Caption, MessageBoxButtons.YesNo);
            return result == DialogResult.Yes;
        }

        public static bool CerrarSistemaParaCambios()
        {
            var msg = "Se necesita ejecutar nuevamente la aplicación para que los cambios sufran efecto, " +
                "si procede la aplicación se cerrara.\nFavor de volver a ejecutarla";
            DialogResult result = MessageBox.Show(msg, Caption, MessageBoxButtons.YesNo);
            return result == DialogResult.Yes;
        }

        public static void ExportadoExitoso()
        {
            var msg = "Generado Exitosamente";
            MensajeExitoso(msg);
        }

        public static void ExportadoDatosInvalidos()
        {
            var msg = "Datos invalidos favor de verificar";
            MensajeAdvertencia(msg);
        }
    }
}