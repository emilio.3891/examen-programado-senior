﻿using System.Configuration;

namespace Conexion
{
    /// <summary>
    ///     Clase para obtener la cadena de conexion del archivo de app.config
    /// </summary>
    public class ConnectionStrings
    {
        /// <summary>
        ///     Constructor para obtener cadena conexion del archivo app.config
        /// </summary>
        /// <param name="name">nombre del clave a buscar</param>
        public ConnectionStrings(string name)
        {
            Connection =
                ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }

        /// <summary>
        ///     Constructor para obtener cadena conexion del archivo app.config
        /// </summary>
        /// <param name="name">nombre del clave a buscar</param>
        /// <param name="user">Usuario para conexion</param>
        /// <param name="password">contraseña para conexion</param>
        public ConnectionStrings(string name, string user, string password)
        {
            Connection = string.Format(
                ConfigurationManager.ConnectionStrings[name].ConnectionString,
                user,
                password);
        }

        /// <summary>
        ///     Cadena de conexion
        /// </summary>
        public string Connection { get; set; }
    }
}