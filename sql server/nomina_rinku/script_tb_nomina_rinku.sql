USE [nomina_rinku]
GO

CREATE TABLE [dbo].[cat_estatus](
	[idu_estatus] [int] NOT NULL PRIMARY KEY,
	[des_estatus] [varchar](50) NOT NULL DEFAULT ('')
)
GO

CREATE TABLE [dbo].[cat_roles](
	[idu_rol] [int] NOT NULL PRIMARY KEY,
	[des_rol] [varchar](100) NOT NULL DEFAULT ('')
)
GO

CREATE TABLE [dbo].[cat_tiposEmpleados](
	[idu_tipoEmpleado] [int] NOT NULL PRIMARY KEY,
	[des_tipoEmpleado] [varchar](100) NOT NULL DEFAULT ('')
)
GO

CREATE TABLE [dbo].[cat_turnos](
	[idu_turno] [int] NOT NULL PRIMARY KEY,
	[des_turno] [varchar](100) NOT NULL DEFAULT ('')
)
GO

CREATE TABLE [dbo].[mae_nominas](
	[num_empleado] [int] NOT NULL,
	[fec_corte] [date] NOT NULL,
	[idu_estatus] [int] NOT NULL,
	[num_mes] [varchar](2) NOT NULL,
	[num_anio] [char](4) NOT NULL,
	[imp_base] [decimal](16, 2) NOT NULL DEFAULT(0),
	[imphrsExtra] [decimal](16, 2) NOT NULL DEFAULT(0),
	[imp_entregas] [decimal](16, 2) NOT NULL DEFAULT(0),
	[imp_bonos] [decimal](16, 2) NOT NULL DEFAULT(0),
	[imp_isr] [decimal](16, 2) NOT NULL DEFAULT(0),
	[imp_isrAdicional] [decimal](16, 2) NOT NULL DEFAULT(0),
	[imp_Imss] [decimal](16, 2) NOT NULL DEFAULT(0),
	[imp_Infonavit] [decimal](16, 2) NOT NULL DEFAULT(0),
	[imp_subTotal] [decimal](16, 2) NOT NULL DEFAULT(0),
	[imp_total] [decimal](16, 2) NOT NULL DEFAULT(0),
	CONSTRAINT [PK_mae_nominas] PRIMARY KEY CLUSTERED 
	(
		[num_empleado] ASC,
		[fec_corte] ASC
	)
)
GO

CREATE TABLE [dbo].[cat_confMonetarias](
	[idu_confMonetaria] [int] NOT NULL PRIMARY KEY,
	[clv_valor] [varchar](50) NOT NULL DEFAULT ('0'),
	[des_confMonetaria] [varchar](100) NOT NULL DEFAULT ('')
)
GO

CREATE TABLE [dbo].[cat_confSistemas](
	[idu_confSistemas] [int] NOT NULL PRIMARY KEY,
	[opc_activo] [char](1) NOT NULL DEFAULT ('0'),
	[des_confSistema] [varchar](100) NOT NULL DEFAULT ('')
)
GO

CREATE TABLE [dbo].[cat_empleados](
	[num_empleado] [int] NOT NULL PRIMARY KEY,
	[nom_empleado] [varchar](50) NOT NULL DEFAULT ('')
)
GO

CREATE TABLE [dbo].[cat_rolesDetalles](
	[idu_rol] [int] NOT NULL,
	[idu_detalle] [int] NOT NULL,
	[idu_confMonetaria] [int] NOT NULL,
	CONSTRAINT [PK_cat_rolesDetalles] PRIMARY KEY CLUSTERED 
	(
		[idu_rol] ASC,
		[idu_detalle] ASC
	)
)
GO

CREATE TABLE [dbo].[cat_empleadosDetalles](
	[num_empleado] [int] NOT NULL PRIMARY KEY,
	[idu_tipoEmpleado] [int] NULL,
	[idu_rol] [int] NULL,
	[clv_imss] [bit] NULL,
	[clv_infonavit] [bit] NULL,
)
GO

CREATE TABLE [dbo].[mov_nominaDetalles](
	[num_empleado] [int] NOT NULL,
	[fec_movimiento] [date] NOT NULL,
	[hrsTrabajadas] [int] NOT NULL,
	[hrsExtras] [int] NOT NULL,
	[numEntregas] [int] NOT NULL,
	[idu_turno] [int] NOT NULL,
	[clv_procesado] [bit ]NOT NULL DEFAULT(0)

	CONSTRAINT [PK_mov_nominaDetalles] PRIMARY KEY CLUSTERED 
	(
		[num_empleado] ASC,
		[fec_movimiento] ASC
	)
)
GO

/****** Object:  ForeignKey [FK_cat_empleadosDetalles_cat_empleados]    Script Date: 03/05/2019 12:29:42 ******/
ALTER TABLE [dbo].[cat_empleadosDetalles]  WITH CHECK ADD  CONSTRAINT [FK_cat_empleadosDetalles_cat_empleados] FOREIGN KEY([num_empleado])
REFERENCES [dbo].[cat_empleados] ([num_empleado])
GO
ALTER TABLE [dbo].[cat_empleadosDetalles] CHECK CONSTRAINT [FK_cat_empleadosDetalles_cat_empleados]
GO
/****** Object:  ForeignKey [FK_cat_empleadosDetalles_cat_roles]    Script Date: 03/05/2019 12:29:42 ******/
ALTER TABLE [dbo].[cat_empleadosDetalles]  WITH CHECK ADD  CONSTRAINT [FK_cat_empleadosDetalles_cat_roles] FOREIGN KEY([idu_rol])
REFERENCES [dbo].[cat_roles] ([idu_rol])
GO
ALTER TABLE [dbo].[cat_empleadosDetalles] CHECK CONSTRAINT [FK_cat_empleadosDetalles_cat_roles]
GO
/****** Object:  ForeignKey [FK_cat_empleadosDetalles_cat_tiposEmpleados]    Script Date: 03/05/2019 12:29:42 ******/
ALTER TABLE [dbo].[cat_empleadosDetalles]  WITH CHECK ADD  CONSTRAINT [FK_cat_empleadosDetalles_cat_tiposEmpleados] FOREIGN KEY([idu_tipoEmpleado])
REFERENCES [dbo].[cat_tiposEmpleados] ([idu_tipoEmpleado])
GO
ALTER TABLE [dbo].[cat_empleadosDetalles] CHECK CONSTRAINT [FK_cat_empleadosDetalles_cat_tiposEmpleados]
GO
/****** Object:  ForeignKey [FK_cat_rolesDetalles_cat_confMonetarias]    Script Date: 03/05/2019 12:29:42 ******/
ALTER TABLE [dbo].[cat_rolesDetalles]  WITH CHECK ADD  CONSTRAINT [FK_cat_rolesDetalles_cat_confMonetarias] FOREIGN KEY([idu_confMonetaria])
REFERENCES [dbo].[cat_confMonetarias] ([idu_confMonetaria])
GO
ALTER TABLE [dbo].[cat_rolesDetalles] CHECK CONSTRAINT [FK_cat_rolesDetalles_cat_confMonetarias]
GO
/****** Object:  ForeignKey [FK_cat_rolesDetalles_cat_roles]    Script Date: 03/05/2019 12:29:42 ******/
ALTER TABLE [dbo].[cat_rolesDetalles]  WITH CHECK ADD  CONSTRAINT [FK_cat_rolesDetalles_cat_roles] FOREIGN KEY([idu_rol])
REFERENCES [dbo].[cat_roles] ([idu_rol])
GO
ALTER TABLE [dbo].[cat_rolesDetalles] CHECK CONSTRAINT [FK_cat_rolesDetalles_cat_roles]
GO
/****** Object:  ForeignKey [FK_mae_nominas_cat_empleados]    Script Date: 03/05/2019 12:29:42 ******/
ALTER TABLE [dbo].[mae_nominas]  WITH CHECK ADD  CONSTRAINT [FK_mae_nominas_cat_empleados] FOREIGN KEY([num_empleado])
REFERENCES [dbo].[cat_empleados] ([num_empleado])
GO
ALTER TABLE [dbo].[mae_nominas] CHECK CONSTRAINT [FK_mae_nominas_cat_empleados]
GO
/****** Object:  ForeignKey [FK_mae_nominas_cat_estatus]    Script Date: 03/05/2019 12:29:42 ******/
ALTER TABLE [dbo].[mae_nominas]  WITH CHECK ADD  CONSTRAINT [FK_mae_nominas_cat_estatus] FOREIGN KEY([idu_estatus])
REFERENCES [dbo].[cat_estatus] ([idu_estatus])
GO
ALTER TABLE [dbo].[mae_nominas] CHECK CONSTRAINT [FK_mae_nominas_cat_estatus]
GO
/****** Object:  ForeignKey [FK_mov_nominaDetalles_cat_empleados]    Script Date: 03/05/2019 12:29:42 ******/
ALTER TABLE [dbo].[mov_nominaDetalles]  WITH CHECK ADD  CONSTRAINT [FK_mov_nominaDetalles_cat_empleados] FOREIGN KEY([num_empleado])
REFERENCES [dbo].[cat_empleados] ([num_empleado])
GO
ALTER TABLE [dbo].[mov_nominaDetalles] CHECK CONSTRAINT [FK_mov_nominaDetalles_cat_empleados]
GO
/****** Object:  ForeignKey [FK_mov_nominaDetalles_cat_turnos]    Script Date: 03/05/2019 12:29:42 ******/
ALTER TABLE [dbo].[mov_nominaDetalles]  WITH CHECK ADD  CONSTRAINT [FK_mov_nominaDetalles_cat_turnos] FOREIGN KEY([idu_turno])
REFERENCES [dbo].[cat_turnos] ([idu_turno])
GO
ALTER TABLE [dbo].[mov_nominaDetalles] CHECK CONSTRAINT [FK_mov_nominaDetalles_cat_turnos]
GO
