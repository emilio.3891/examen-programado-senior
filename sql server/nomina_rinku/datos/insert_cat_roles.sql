USE [nomina_rinku]
GO

INSERT INTO [dbo].[cat_roles]
    ([idu_rol] ,[des_rol])
VALUES
    (1, 'Chofer'),
    (2, 'Cargador'),
    (3, 'Auxiliar');
GO
