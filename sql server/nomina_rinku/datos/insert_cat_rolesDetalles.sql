USE [nomina_rinku]
GO

INSERT INTO [dbo].[cat_rolesDetalles]
    ([idu_rol] ,[idu_detalle] ,[idu_confMonetaria])
VALUES
    (1, 1, 1),
    (1, 2, 4),
    (1, 3, 7),
    (1, 4, 15),
    (2, 1, 2),
    (2, 2, 5),
    (2, 3, 8),
    (2, 4, 16),
    (3, 1, 3),
    (3, 2, 6),
    (3, 3, 9),
    (3, 4, 17);
GO
