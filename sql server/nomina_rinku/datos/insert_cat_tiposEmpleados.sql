USE [nomina_rinku]
GO

INSERT INTO [dbo].[cat_tiposEmpleados]
    ([idu_tipoEmpleado] ,[des_tipoEmpleado])
VALUES
    (1 ,'Interno'),
    (2 ,'Sub-Contratado');
GO
