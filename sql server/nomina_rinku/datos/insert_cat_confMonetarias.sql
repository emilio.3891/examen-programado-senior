USE [nomina_rinku]
GO

INSERT INTO [dbo].[cat_confMonetarias]
    ([idu_confMonetaria] ,[clv_valor] ,[des_confMonetaria])
VALUES
    (1, '30.00', 'Sueldo Base Chofer'),
    (2, '30.00', 'Sueldo Base Cargador'),
    (3, '30.00', 'Sueldo Base Auxiliar'),
    (4, '5.00', 'Adicional por entrega Chofer'),
    (5, '5.00', 'Adicional por entrega Cargador'),
    (6, '5.00', 'Adicional por entrega Auxiliar'),
    (7, '10.00', 'Sueldo Adicional Chofer'),
    (8, '5.00', 'Sueldo Adicional Cargador'),
    (9, '0.00', 'Sueldo Adicional Auxiliar'),
    (10, '0.09', 'Porcentaje por ISR'),
    (11, '0.03', 'Porcentaje por ISR Adicional'),
    (12, '0.04', 'Porcentaje para despensa'),
    (13, '0.0284', 'Porcentaje para IMSS'),
    (14, '0.1368', 'Porcentaje para INFONAVIT'),
    (15, '1.00', 'Porcentaje Hora Extra Chofer'),
    (16, '1.00', 'Porcentaje Hora Extra Cargador'),
    (17, '1.00', 'Porcentaje Hora Extra Auxiliar');
GO
