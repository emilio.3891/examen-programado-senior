USE [nomina_rinku]
GO

INSERT INTO [dbo].[cat_estatus]
    ([idu_estatus] ,[des_estatus])
VALUES
    (0 ,'Procesando Corte'),
    (1 ,'Por pagar'),
    (2 ,'Pagado'),
    (3 ,'Detenido')
GO
