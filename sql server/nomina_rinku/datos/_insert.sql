USE [nomina_rinku]
GO

DELETE FROM mae_nominas;
DELETE FROM cat_rolesDetalles;
DELETE FROM cat_empleadosDetalles;
DELETE FROM mov_nominaDetalles;
DELETE FROM cat_tiposEmpleados;
DELETE FROM cat_estatus;
DELETE FROM cat_roles;
DELETE FROM cat_turnos;
DELETE FROM cat_empleados;
DELETE FROM cat_confMonetarias;
DELETE FROM cat_confSistemas;
GO

USE [nomina_rinku]
GO
INSERT INTO [dbo].[cat_confMonetarias]
    ([idu_confMonetaria] ,[clv_valor] ,[des_confMonetaria])
VALUES
    (1, '30.00', 'Sueldo Base Chofer'),
    (2, '30.00', 'Sueldo Base Cargador'),
    (3, '30.00', 'Sueldo Base Auxiliar'),
    (4, '5.00', 'Adicional por entrega Chofer'),
    (5, '5.00', 'Adicional por entrega Cargador'),
    (6, '5.00', 'Adicional por entrega Auxiliar'),
    (7, '10.00', 'Sueldo Adicional Chofer'),
    (8, '5.00', 'Sueldo Adicional Cargador'),
    (9, '0.00', 'Sueldo Adicional Auxiliar'),
    (10, '0.09', 'Porcentaje por ISR'),
    (11, '0.03', 'Porcentaje por ISR Adicional'),
    (12, '0.04', 'Porcentaje para despensa'),
    (13, '0.0284', 'Porcentaje para IMSS'),
    (14, '0.1368', 'Porcentaje para INFONAVIT'),
    (15, '1.00', 'Porcentaje Hora Extra Chofer'),
    (16, '1.00', 'Porcentaje Hora Extra Cargador'),
    (17, '1.00', 'Porcentaje Hora Extra Auxiliar');
GO
USE [nomina_rinku]
GO

INSERT INTO [dbo].[cat_confSistemas]
    ([idu_confSistemas] ,[opc_activo] ,[des_confSistema])
VALUES
    (1, '0', 'Activar hora extra'),
    (2, '0', 'Activar IMSS'),
    (3, '0', 'Activar Infonavit');
GO
USE [nomina_rinku]
GO

INSERT INTO [dbo].[cat_estatus]
    ([idu_estatus] ,[des_estatus])
VALUES
    (0 ,'Procesando Corte'),
    (1 ,'Por pagar'),
    (2 ,'Pagado'),
    (3 ,'Detenido')
GO
USE [nomina_rinku]
GO

INSERT INTO [dbo].[cat_roles]
    ([idu_rol] ,[des_rol])
VALUES
    (1, 'Chofer'),
    (2, 'Cargador'),
    (3, 'Auxiliar');
GO
USE [nomina_rinku]
GO

INSERT INTO [dbo].[cat_rolesDetalles]
    ([idu_rol] ,[idu_detalle] ,[idu_confMonetaria])
VALUES
    (1, 1, 1),
    (1, 2, 4),
    (1, 3, 7),
    (1, 4, 15),
    (2, 1, 2),
    (2, 2, 5),
    (2, 3, 8),
    (2, 4, 16),
    (3, 1, 3),
    (3, 2, 6),
    (3, 3, 9),
    (3, 4, 17);
GO
USE [nomina_rinku]
GO

INSERT INTO [dbo].[cat_tiposEmpleados]
    ([idu_tipoEmpleado] ,[des_tipoEmpleado])
VALUES
    (1 ,'Interno'),
    (2 ,'Sub-Contratado');
GO
USE [nomina_rinku]
GO

INSERT INTO [dbo].[cat_turnos]
    ([idu_turno] ,[des_turno])
VALUES
    (0, 'No cubrio turno'),
    (1, 'Cubrio Chofer'),
    (2, 'Cubrio Cargador');
GO
