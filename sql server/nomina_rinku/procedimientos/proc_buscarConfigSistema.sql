USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_buscarConfigSistema]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_buscarConfigSistema]
END
GO

CREATE PROCEDURE [dbo].[proc_buscarConfigSistema]
    (@idu_confSistemas INT = NULL
    ,@des_confSistema VARCHAR(100))
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Busca las configuraciones del sistema
-- si ambos parametros son nulos se retorna toda la
-- informacion, si no se regresa de acuerdo
-- al dato no nulo priorizando el primer dato
-- Versión: 20190307.1031
-- =============================================
BEGIN
    IF(@idu_confSistemas IS NOT NULL)
    BEGIN
        SELECT [idu_confSistemas]
            ,[opc_activo]
            ,[des_confSistema]
        FROM [nomina_rinku].[dbo].[cat_confSistemas]
        WHERE
            CAST([idu_confSistemas] AS VARCHAR)
                LIKE '%' + CAST(@idu_confSistemas AS VARCHAR) + '%'
    END
    ELSE
    BEGIN
        SELECT [idu_confSistemas]
            ,[opc_activo]
            ,[des_confSistema]
        FROM [nomina_rinku].[dbo].[cat_confSistemas]
        WHERE [des_confSistema] LIKE '%' + @des_confSistema + '%'
    END
END
GO
