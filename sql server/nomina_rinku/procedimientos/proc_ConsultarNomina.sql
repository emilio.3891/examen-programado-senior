USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_ConsultarNomina]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_ConsultarNomina]
END
GO

CREATE PROCEDURE [dbo].[proc_ConsultarNomina]
    (@num_empleado INT
    ,@num_mes VARCHAR(2)
    ,@num_anio CHAR(4))
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	sp que se encarga de generar la informacion
-- de empleado sobre su nomina de acuerdo al mes,
-- año y num_empleado
-- Versión: 20190307.0841
-- =============================================
BEGIN
    DECLARE @clv_imss CHAR(1);
    DECLARE @clv_hrsExtra CHAR(1);
    DECLARE @clv_Infonavit CHAR(1);

    SELECT
    @clv_hrsExtra = CASE WHEN idu_confSistemas = 1 THEN opc_activo ELSE @clv_hrsExtra END
    ,@clv_imss = CASE WHEN idu_confSistemas = 2 THEN opc_activo ELSE '0' END
    ,@clv_Infonavit = CASE WHEN idu_confSistemas = 3 THEN opc_activo ELSE '0' END
    FROM [cat_confSistemas];

    IF(@clv_hrsExtra = '1')
    BEGIN
        SELECT 'HORA EXTRA'
    END

    IF(@clv_imss = '1')
    BEGIN
        SELECT 'HORA @clv_imss'
    END

    IF(@clv_Infonavit = '1')
    BEGIN
        SELECT 'HORA @clv_Infonavit'
    END

    SELECT [num_empleado]
    ,[fec_corte]
    ,[idu_estatus]
    ,[num_mes]
    ,[num_anio]
    ,[imp_base]
    ,[imphrsExtra]
    ,[imp_entregas]
    ,[imp_bonos]
    ,[imp_isr]
    ,[imp_isrAdicional]
    ,[imp_Imss]
    ,[imp_Infonavit]
    ,[imp_subTotal]
    ,[imp_total]
    FROM [nomina_rinku].[dbo].[mae_nominas]
    WHERE
        [num_mes] = @num_mes
        AND [num_anio] = @num_anio
END
GO
