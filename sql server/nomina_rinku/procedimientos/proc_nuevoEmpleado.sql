USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_nuevoEmpleado]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_nuevoEmpleado]
END
GO

CREATE PROCEDURE [dbo].[proc_nuevoEmpleado]
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Retorna el id maximo +1
-- Versión: 20190307.0901
-- =============================================
BEGIN
    SELECT
        (ISNULL(MAX(num_empleado), 0) + 1) AS num_empleado
    FROM cat_empleados
END
GO
