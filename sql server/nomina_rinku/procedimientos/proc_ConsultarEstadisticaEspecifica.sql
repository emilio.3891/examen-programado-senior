USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_ConsultarEstadisticaEspecifica]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_ConsultarEstadisticaEspecifica]
END
GO

CREATE PROCEDURE [dbo].[proc_ConsultarEstadisticaEspecifica]
    (@fechaInicio date
    ,@fechaFinal date)
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Consulta estadisticas especificas, devolviendo
-- todos los datos utiles
-- Versión: 20190307.1151
-- =============================================
BEGIN
    SELECT
        n.num_empleado
        ,emp.nom_empleado
        ,num_mes
        ,num_anio
        ,fec_corte
        ,imp_subTotal
        ,imp_total
        ,e.des_estatus
    FROM nomina_rinku.dbo.mae_nominas n
    INNER JOIN cat_empleados emp ON emp.num_empleado = n.num_empleado
    INNER JOIN cat_estatus e ON n.idu_estatus = e.idu_estatus
END
GO
