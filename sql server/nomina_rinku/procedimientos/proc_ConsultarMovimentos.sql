USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_ConsultarMovimentos]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_ConsultarMovimentos]
END
GO

CREATE PROCEDURE [dbo].[proc_ConsultarMovimentos]
    (@fecha date)
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Consulta Movimientos de una fecha
-- Versión: 20190307.0841
-- =============================================
BEGIN
    
    IF OBJECT_ID('tempdb.dbo.#mov', 'U') IS NOT NULL   
    BEGIN
        DROP TABLE #mov
    END
    SELECT
        @fecha mov_nominaDetalles
        ,emp.num_empleado
        ,emp.nom_empleado
        ,rol.idu_rol
        ,rol.des_rol des_rol
        ,CAST('0' AS VARCHAR(100)) hrsTrabajadas
        ,CAST('0' AS VARCHAR(100)) numEntregas
        ,CAST('0' AS VARCHAR(100)) idu_turno
        ,CAST('0' AS VARCHAR(100)) hrsExtras
    INTO #mov
    FROM cat_empleados emp
    INNER JOIN cat_empleadosDetalles det ON  emp.num_empleado = det.num_empleado
    INNER JOIN cat_roles rol ON det.idu_rol = rol.idu_rol

    UPDATE m SET
        m.hrsTrabajadas = mov_nominaDetalles.hrsTrabajadas
        ,m.numEntregas = mov_nominaDetalles.numEntregas
        ,m.idu_turno = mov_nominaDetalles.idu_turno
        ,m.hrsExtras = mov_nominaDetalles.hrsExtras
    from #mov m
    LEFT JOIN mov_nominaDetalles  ON m.num_empleado = mov_nominaDetalles.num_empleado 
    FULL OUTER JOIN cat_empleados ON mov_nominaDetalles.num_empleado = cat_empleados.num_empleado 
    INNER JOIN cat_empleadosDetalles ON cat_empleados.num_empleado = cat_empleadosDetalles.num_empleado 
    LEFT JOIN cat_turnos ON mov_nominaDetalles.idu_turno =  cat_turnos.idu_turno
    WHERE mov_nominaDetalles.fec_movimiento = @fecha

    SELECT * FROM #mov
END
GO
