USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_guardarEmpleado]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_guardarEmpleado]
END
GO

CREATE PROCEDURE [dbo].[proc_guardarEmpleado]
    (@num_empleado int
    ,@nom_empleado varchar(50)
    ,@idu_tipoEmpleado int
    ,@idu_rol int
    ,@clv_imss bit
    ,@clv_infonavit bit)
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Guarda un empleado nuevo si no existe,
-- si ya existe actualiza sus datos.
-- Versión: 20190307.0841
-- =============================================
BEGIN
    UPDATE [nomina_rinku].[dbo].[cat_empleados]
    SET [nom_empleado] = @nom_empleado
    WHERE num_empleado = @num_empleado

    UPDATE [nomina_rinku].[dbo].[cat_empleadosDetalles]
    SET [idu_tipoEmpleado] = @idu_tipoEmpleado
        ,[idu_rol] = @idu_rol
        ,[clv_imss] = @clv_imss
        ,[clv_infonavit] = @clv_infonavit
    WHERE [num_empleado] = @num_empleado

    IF(@@ROWCOUNT = 0)
    BEGIN
        INSERT INTO [nomina_rinku].[dbo].[cat_empleados]
            ([num_empleado] ,[nom_empleado])
        VALUES
            (@num_empleado ,@nom_empleado);

        INSERT INTO [dbo].[cat_empleadosDetalles]
            ([num_empleado] ,[idu_tipoEmpleado] ,[idu_rol] ,[clv_imss] ,[clv_infonavit])
        VALUES
            (@num_empleado
            ,@idu_tipoEmpleado
            ,@idu_rol
            ,@clv_imss
            ,@clv_infonavit);
    END
END
GO
