USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('nomina_rinku.dbo.proc_corteMensual', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.proc_corteMensual
END
GO

CREATE PROCEDURE dbo.proc_corteMensual
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description: Se encarga de realizar el corte mensual
-- Versión: 20190307.0841
-- =============================================
BEGIN
    DECLARE	@fecha date = GETDATE();
    DECLARE @fechaCorte date;
    DECLARE @fechaCorteAnterior date;
    SELECT @fechaCorte =
        DateAdd(DAY, 21 - 1,
        DateAdd(MONTH, MONTH(@fecha) - 1,
        DateAdd(YEAR, YEAR(@fecha)-1900, 0)))
	SELECT @fechaCorteAnterior = DATEADD(MONTH,-1,@fechaCorte)
	SELECT @fechaCorte,@fechaCorteAnterior
	
	DELETE FROM nomina_rinku.dbo.mae_nominas
	WHERE num_mes = YEAR(@fechaCorte) 
		AND num_anio = MONTH(@fechaCorte)
    INSERT INTO nomina_rinku.dbo.mae_nominas
        (num_empleado
        ,fec_corte
        ,idu_estatus
        ,num_mes
        ,num_anio
        ,imp_base
        ,imphrsExtra
        ,imp_entregas
        ,imp_bonos
        ,imp_isr
        ,imp_isrAdicional
        ,imp_Imss
        ,imp_Infonavit
        ,imp_subTotal
        ,imp_total)
    SELECT
        num_empleado AS num_empleado
        ,@fechaCorte AS fec_corte
        ,0 AS idu_estatus
        ,MONTH(@fechaCorte) AS num_mes
        ,YEAR(@fechaCorte) AS num_anio
        ,SUM(hrsTrabajadas) AS imp_base
        ,SUM(hrsExtras) AS imphrsExtra
        ,SUM(numEntregas) AS imp_entregas
        ,0 AS imp_bonos
        ,0 AS imp_isr
        ,0 AS imp_isrAdicional
        ,0 AS imp_Imss
        ,0 AS imp_Infonavit
        ,0 AS imp_subTotal
        ,0 AS imp_total
    FROM nomina_rinku.dbo.mov_nominaDetalles
    WHERE 
		fec_movimiento < @fechaCorte
        AND fec_movimiento >= @fechaCorteAnterior
        GROUP BY num_empleado

    -------------------------Ingresos-------------------------
    --Salario Base
    UPDATE nomina SET
        imp_base = (nomina.imp_base * emp.clv_valor)
    FROM nomina_rinku.dbo.mae_nominas nomina
    INNER JOIN vis_empleados emp ON emp.num_empleado = nomina.num_empleado
    WHERE
        nomina.Idu_estatus = 0
        AND idu_detalle = 1

    --Salario por Entregas
    UPDATE nomina SET
        imp_entregas = (nomina.imp_entregas * emp.clv_valor)
    FROM nomina_rinku.dbo.mae_nominas nomina
    INNER JOIN vis_empleados emp ON emp.num_empleado = nomina.num_empleado
    WHERE
        nomina.Idu_estatus = 0
        AND idu_detalle = 2

    --Salario por Bono
    UPDATE nomina SET
        imp_bonos = (nomina.imp_bonos * emp.clv_valor)
    FROM nomina_rinku.dbo.mae_nominas nomina
    INNER JOIN vis_empleados emp ON emp.num_empleado = nomina.num_empleado
    WHERE
        nomina.Idu_estatus = 0
        AND idu_detalle = 3
        AND idu_tipoEmpleado = 1

    --SubTotal
    UPDATE nomina SET
        imp_subTotal = imp_base +((imp_entregas + imp_bonos) + imphrsExtra)
    FROM nomina_rinku.dbo.mae_nominas nomina
    INNER JOIN cat_empleadosDetalles emp ON emp.num_empleado = nomina.num_empleado
    WHERE
        nomina.Idu_estatus = 0
    -------------------------Egresos-------------------------
    --ISR
    UPDATE nomina SET
        imp_isr = nomina.imp_subTotal * (CAST(conf.clv_valor as FLOAT))
    FROM nomina_rinku.dbo.mae_nominas nomina
    INNER JOIN cat_empleadosDetalles emp ON emp.num_empleado = nomina.num_empleado
    INNER JOIN cat_confMonetarias conf ON conf.idu_confMonetaria = 10
    WHERE
        nomina.Idu_estatus = 0
    --ISR Adicional
    UPDATE nomina SET
        imp_isrAdicional = nomina.imp_subTotal * (CAST(conf.clv_valor as FLOAT))
    FROM nomina_rinku.dbo.mae_nominas nomina
    INNER JOIN cat_empleadosDetalles emp ON emp.num_empleado = nomina.num_empleado
    INNER JOIN cat_confMonetarias conf ON conf.idu_confMonetaria = 11
    WHERE
        nomina.Idu_estatus = 0
        AND nomina.imp_subTotal > 16000

    -------------------------Procesos Adicionales-------------------------
    --Hora Extra
    IF EXISTS (SELECT TOP 1 1 FROM cat_confSistemas
                WHERE idu_confSistemas = 1 AND opc_activo=1)
    BEGIN
        UPDATE nomina SET
            imphrsExtra = (nomina.imphrsExtra * (1 + CAST(emp.clv_valor AS FLOAT)))
        FROM nomina_rinku.dbo.mae_nominas nomina
        INNER JOIN vis_empleados emp ON emp.num_empleado = nomina.num_empleado
        WHERE
            nomina.Idu_estatus = 0
            AND idu_detalle = 4
            AND idu_tipoEmpleado = 1
    END
    --IMSS
    IF EXISTS (SELECT TOP 1 1 FROM cat_confSistemas
                WHERE idu_confSistemas = 2 AND opc_activo = 1)
    BEGIN
        UPDATE nomina SET
            imp_Imss = nomina.imp_subTotal * (CAST(conf.clv_valor as FLOAT))
        FROM nomina_rinku.dbo.mae_nominas nomina
        INNER JOIN cat_empleadosDetalles emp ON emp.num_empleado = nomina.num_empleado
        INNER JOIN cat_confMonetarias conf ON conf.idu_confMonetaria = 13
        WHERE
            nomina.Idu_estatus = 0
            AND emp.clv_imss = 1
    END
    --Infonavit
    IF EXISTS (SELECT TOP 1 1 FROM cat_confSistemas
                WHERE idu_confSistemas = 3 AND opc_activo = 1)
    BEGIN
        UPDATE nomina SET
            imp_Infonavit = nomina.imp_subTotal * (CAST(clv_valor AS FLOAT))
        FROM nomina_rinku.dbo.mae_nominas nomina
        INNER JOIN cat_empleadosDetalles emp ON emp.num_empleado = nomina.num_empleado
        INNER JOIN cat_confMonetarias conf ON conf.idu_confMonetaria = 14
        WHERE
            nomina.Idu_estatus = 0
            AND emp.clv_infonavit = 1
    END
    -------------------------Finalizar Proceso-------------------------
    UPDATE nomina SET
        imp_total = imp_subTotal - ((imp_isr + imp_isrAdicional) + imp_Imss + imp_Infonavit)
    FROM nomina_rinku.dbo.mae_nominas nomina
    WHERE
        nomina.Idu_estatus = 0

    UPDATE mov SET
        clv_procesado = 1
    FROM nomina_rinku.dbo.mov_nominaDetalles mov

    UPDATE nomina SET
        idu_estatus = 1
    FROM nomina_rinku.dbo.mae_nominas nomina
END
GO
