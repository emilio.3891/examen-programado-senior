USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_buscarMovimientos]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_buscarMovimientos]
END
GO

CREATE PROCEDURE [dbo].[proc_buscarMovimientos]
    (@num_empleado INT
    ,@fec_movimiento DATE)
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Busca un movimientos especifico por
-- num_empleado y fec_movimiento
-- Versión: 20190307.0957
-- =============================================
BEGIN
    SELECT
        [num_empleado]
        ,[fec_movimiento]
        ,[hrsTrabajadas]
        ,[hrsExtras]
        ,[numEntregas]
        ,[idu_turno]
        ,[clv_procesado]
    FROM [nomina_rinku].[dbo].[mov_nominaDetalles]
    WHERE
        [num_empleado] = @num_empleado
        AND [fec_movimiento] = @fec_movimiento
END
GO
