USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_guardarConfigMonetaria]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_guardarConfigMonetaria]
END
GO

CREATE PROCEDURE [dbo].[proc_guardarConfigMonetaria]
    (@idu_confMonetaria INT
    ,@clv_valor VARCHAR(50))
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	sp para actualizar valores configurables
-- Versión: 20190307.0841
-- =============================================
BEGIN
    UPDATE [nomina_rinku].[dbo].[cat_confMonetarias]
    SET [clv_valor] = @clv_valor
    WHERE [idu_confMonetaria] = @idu_confMonetaria
END
GO
