USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_consultarTurnos]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_consultarTurnos]
END
GO

CREATE PROCEDURE [dbo].[proc_consultarTurnos]
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Retorna los diversos Turnos
-- Versión: 20190307.1032
-- =============================================
BEGIN
    SELECT
        [idu_turno]
        ,[des_turno]
    FROM [nomina_rinku].[dbo].[cat_turnos]
END
GO
