USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_guardarMovimientos]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_guardarMovimientos]
END
GO

CREATE PROCEDURE [dbo].[proc_guardarMovimientos]
    (@num_empleado INT
    ,@fec_movimiento DATE
    ,@hrsTrabajadas INT
    ,@hrsExtras INT
    ,@numEntregas INT
    ,@idu_turno INT)
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Guarda un movimiento nuevo si no existe,
-- si ya existe actualiza sus datos.
-- Versión: 20190307.0841
-- =============================================
BEGIN
    UPDATE [nomina_rinku].[dbo].[mov_nominaDetalles]
    SET  [num_empleado] = @num_empleado
        ,[fec_movimiento] = @fec_movimiento
        ,[hrsTrabajadas] = @hrsTrabajadas
        ,[hrsExtras] = @hrsExtras
        ,[numEntregas] = @numEntregas
        ,[idu_turno] = @idu_turno
    WHERE   [num_empleado] = @num_empleado
        AND [fec_movimiento] = @fec_movimiento;

    IF(@@ROWCOUNT = 0)
    BEGIN
        INSERT INTO [nomina_rinku].[dbo].[mov_nominaDetalles]
            ([num_empleado]
            ,[fec_movimiento]
            ,[hrsTrabajadas]
            ,[hrsExtras]
            ,[numEntregas]
            ,[idu_turno])
        VALUES
            (@num_empleado
            ,@fec_movimiento
            ,@hrsTrabajadas
            ,@hrsExtras
            ,@numEntregas
            ,@idu_turno);
    END
END
GO
