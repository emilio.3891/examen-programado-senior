USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_buscarConfigMonetaria]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_buscarConfigMonetaria]
END
GO

CREATE PROCEDURE [dbo].[proc_buscarConfigMonetaria]
    (@des_confMonetaria VARCHAR(100))
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Busca las configuraciones del monetarias
-- si ambos parametros son nulos se retorna toda la
-- informacion, si no se regresa de acuerdo
-- al dato no nulo priorizando el primer dato
-- Versión: 20190307.1032
-- =============================================
BEGIN
    SELECT [idu_confMonetaria]
        ,[clv_valor]
        ,[des_confMonetaria]
    FROM [nomina_rinku].[dbo].[cat_confMonetarias]
    WHERE [des_confMonetaria] LIKE '%' + @des_confMonetaria + '%'
END
GO
