USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_guardarConfigSistema]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_guardarConfigSistema]
END
GO

CREATE PROCEDURE [dbo].[proc_guardarConfigSistema]
    (@idu_confSistemas INT
    ,@opc_activo CHAR(1))
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Actualiza las opciones especiales del sistema
-- Versión: 20190307.0841
-- =============================================
BEGIN
    UPDATE [nomina_rinku].[dbo].[cat_confSistemas]
    SET [opc_activo] = @opc_activo
    WHERE
        [idu_confSistemas] = @idu_confSistemas
END
GO
