USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_consultarRoles]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_consultarRoles]
END
GO

CREATE PROCEDURE [dbo].[proc_consultarRoles]
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Retorna los diversos roles
-- Versión: 20190307.1032
-- =============================================
BEGIN
    SELECT
        [idu_rol]
        ,[des_rol]
    FROM [dbo].[cat_roles]
END
GO
