USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_buscarConfigMonetaria]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_buscarConfigMonetaria]
END
GO

CREATE PROCEDURE [dbo].[proc_buscarConfigMonetaria]
    (@des_confMonetaria VARCHAR(100))
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Busca las configuraciones del monetarias
-- si ambos parametros son nulos se retorna toda la
-- informacion, si no se regresa de acuerdo
-- al dato no nulo priorizando el primer dato
-- Versión: 20190307.1032
-- =============================================
BEGIN
    SELECT [idu_confMonetaria]
        ,[clv_valor]
        ,[des_confMonetaria]
    FROM [nomina_rinku].[dbo].[cat_confMonetarias]
    WHERE [des_confMonetaria] LIKE '%' + @des_confMonetaria + '%'
END
GO

USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_buscarConfigSistema]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_buscarConfigSistema]
END
GO

CREATE PROCEDURE [dbo].[proc_buscarConfigSistema]
    (@idu_confSistemas INT = NULL
    ,@des_confSistema VARCHAR(100))
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Busca las configuraciones del sistema
-- si ambos parametros son nulos se retorna toda la
-- informacion, si no se regresa de acuerdo
-- al dato no nulo priorizando el primer dato
-- Versión: 20190307.1031
-- =============================================
BEGIN
    IF(@idu_confSistemas IS NOT NULL)
    BEGIN
        SELECT [idu_confSistemas]
            ,[opc_activo]
            ,[des_confSistema]
        FROM [nomina_rinku].[dbo].[cat_confSistemas]
        WHERE
            CAST([idu_confSistemas] AS VARCHAR)
                LIKE '%' + CAST(@idu_confSistemas AS VARCHAR) + '%'
    END
    ELSE
    BEGIN
        SELECT [idu_confSistemas]
            ,[opc_activo]
            ,[des_confSistema]
        FROM [nomina_rinku].[dbo].[cat_confSistemas]
        WHERE [des_confSistema] LIKE '%' + @des_confSistema + '%'
    END
END
GO
USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_buscarEmpleado]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_buscarEmpleado]
END
GO

CREATE PROCEDURE [dbo].[proc_buscarEmpleado]
    (@num_empleado INT
    ,@nom_empleado VARCHAR(50))
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:
-- Versión: 20190307.0841
-- =============================================
BEGIN
    IF (@num_empleado IS NULL)
    BEGIN
        SELECT
            [emp].[num_empleado],
            [emp].[nom_empleado],
            [det].[idu_tipoEmpleado],
            [det].[idu_rol],
            [det].[clv_imss],
            [det].[clv_infonavit]
        FROM [cat_empleados] emp
        INNER JOIN [cat_empleadosDetalles] det
            ON [emp].[num_empleado] = [det].[num_empleado]
        WHERE [emp].[nom_empleado] LIKE '%' + @nom_empleado + '%';
    END
    ELSE
    BEGIN
        SELECT
            [emp].[num_empleado],
            [emp].[nom_empleado],
            [det].[idu_tipoEmpleado],
            [det].[idu_rol],
            [det].[clv_imss],
            [det].[clv_infonavit]
        FROM [cat_empleados] emp
        INNER JOIN [cat_empleadosDetalles] det
            ON [emp].[num_empleado] = [det].[num_empleado]
        WHERE [emp].[num_empleado] = @num_empleado;
    END
END
GO
USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_buscarEmpleadoDetalle]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_buscarEmpleadoDetalle]
END
GO

CREATE PROCEDURE [dbo].[proc_buscarEmpleadoDetalle]
    (@num_empleado INT)
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	
-- Versión: 20190307.1032
-- =============================================
BEGIN
    SELECT DISTINCT 
        [num_empleado]
        ,[nom_empleado]
        ,[idu_tipoEmpleado]
        ,[des_tipoEmpleado]
        ,[idu_rol]
        ,[des_rol]
    FROM [nomina_rinku].[dbo].[vis_empleados]
    WHERE [num_empleado] = @num_empleado
END
GO
USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_buscarMovimientos]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_buscarMovimientos]
END
GO

CREATE PROCEDURE [dbo].[proc_buscarMovimientos]
    (@num_empleado INT
    ,@fec_movimiento DATE)
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Busca un movimientos especifico por
-- num_empleado y fec_movimiento
-- Versión: 20190307.0957
-- =============================================
BEGIN
    SELECT
        [num_empleado]
        ,[fec_movimiento]
        ,[hrsTrabajadas]
        ,[hrsExtras]
        ,[numEntregas]
        ,[idu_turno]
        ,[clv_procesado]
    FROM [nomina_rinku].[dbo].[mov_nominaDetalles]
    WHERE
        [num_empleado] = @num_empleado
        AND [fec_movimiento] = @fec_movimiento
END
GO
USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_ConsultarEstadisticaEspecifica]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_ConsultarEstadisticaEspecifica]
END
GO

CREATE PROCEDURE [dbo].[proc_ConsultarEstadisticaEspecifica]
    (@fechaInicio date
    ,@fechaFinal date)
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Consulta estadisticas especificas, devolviendo
-- todos los datos utiles
-- Versión: 20190307.1151
-- =============================================
BEGIN
    SELECT
        n.num_empleado
        ,emp.nom_empleado
        ,num_mes
        ,num_anio
        ,fec_corte
        ,imp_subTotal
        ,imp_total
        ,e.des_estatus
    FROM nomina_rinku.dbo.mae_nominas n
    INNER JOIN cat_empleados emp ON emp.num_empleado = n.num_empleado
    INNER JOIN cat_estatus e ON n.idu_estatus = e.idu_estatus
END
GO
USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_ConsultarEstadisticaGeneral]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_ConsultarEstadisticaGeneral]
END
GO

CREATE PROCEDURE [dbo].[proc_ConsultarEstadisticaGeneral]
    (@fechaInicio date
    ,@fechaFinal date)
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Retorna estadisticas generales totales principalmente
-- Versión: 20190307.1151
-- =============================================
BEGIN
    SELECT
        n.num_empleado
        ,emp.nom_empleado
        ,num_mes
        ,num_anio
        ,fec_corte
        ,imp_subTotal
        ,imp_total
        ,e.des_estatus
    FROM nomina_rinku.dbo.mae_nominas n
    INNER JOIN cat_empleados emp ON emp.num_empleado = n.num_empleado
    INNER JOIN cat_estatus e ON n.idu_estatus = e.idu_estatus
    WHERE fec_corte BETWEEN @fechaInicio AND @fechaFinal
    ORDER BY fec_corte;
END
GO
USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_ConsultarMovimentos]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_ConsultarMovimentos]
END
GO

CREATE PROCEDURE [dbo].[proc_ConsultarMovimentos]
    (@fecha date)
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Consulta Movimientos de una fecha
-- Versión: 20190307.0841
-- =============================================
BEGIN
    
    IF OBJECT_ID('tempdb.dbo.#mov', 'U') IS NOT NULL   
    BEGIN
        DROP TABLE #mov
    END
    SELECT
        @fecha mov_nominaDetalles
        ,emp.num_empleado
        ,emp.nom_empleado
        ,rol.idu_rol
        ,rol.des_rol des_rol
        ,CAST('0' AS VARCHAR(100)) hrsTrabajadas
        ,CAST('0' AS VARCHAR(100)) numEntregas
        ,CAST('0' AS VARCHAR(100)) idu_turno
        ,CAST('0' AS VARCHAR(100)) hrsExtras
    INTO #mov
    FROM cat_empleados emp
    INNER JOIN cat_empleadosDetalles det ON  emp.num_empleado = det.num_empleado
    INNER JOIN cat_roles rol ON det.idu_rol = rol.idu_rol

    UPDATE m SET
        m.hrsTrabajadas = mov_nominaDetalles.hrsTrabajadas
        ,m.numEntregas = mov_nominaDetalles.numEntregas
        ,m.idu_turno = mov_nominaDetalles.idu_turno
        ,m.hrsExtras = mov_nominaDetalles.hrsExtras
    from #mov m
    LEFT JOIN mov_nominaDetalles  ON m.num_empleado = mov_nominaDetalles.num_empleado 
    FULL OUTER JOIN cat_empleados ON mov_nominaDetalles.num_empleado = cat_empleados.num_empleado 
    INNER JOIN cat_empleadosDetalles ON cat_empleados.num_empleado = cat_empleadosDetalles.num_empleado 
    LEFT JOIN cat_turnos ON mov_nominaDetalles.idu_turno =  cat_turnos.idu_turno
    WHERE mov_nominaDetalles.fec_movimiento = @fecha

    SELECT * FROM #mov
END
GO
USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_ConsultarNomina]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_ConsultarNomina]
END
GO

CREATE PROCEDURE [dbo].[proc_ConsultarNomina]
    (@num_empleado INT
    ,@num_mes VARCHAR(2)
    ,@num_anio CHAR(4))
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	sp que se encarga de generar la informacion
-- de empleado sobre su nomina de acuerdo al mes,
-- año y num_empleado
-- Versión: 20190307.0841
-- =============================================
BEGIN
    DECLARE @clv_imss CHAR(1);
    DECLARE @clv_hrsExtra CHAR(1);
    DECLARE @clv_Infonavit CHAR(1);

    SELECT
    @clv_hrsExtra = CASE WHEN idu_confSistemas = 1 THEN opc_activo ELSE @clv_hrsExtra END
    ,@clv_imss = CASE WHEN idu_confSistemas = 2 THEN opc_activo ELSE '0' END
    ,@clv_Infonavit = CASE WHEN idu_confSistemas = 3 THEN opc_activo ELSE '0' END
    FROM [cat_confSistemas];

    IF(@clv_hrsExtra = '1')
    BEGIN
        SELECT 'HORA EXTRA'
    END

    IF(@clv_imss = '1')
    BEGIN
        SELECT 'HORA @clv_imss'
    END

    IF(@clv_Infonavit = '1')
    BEGIN
        SELECT 'HORA @clv_Infonavit'
    END

    SELECT [num_empleado]
    ,[fec_corte]
    ,[idu_estatus]
    ,[num_mes]
    ,[num_anio]
    ,[imp_base]
    ,[imphrsExtra]
    ,[imp_entregas]
    ,[imp_bonos]
    ,[imp_isr]
    ,[imp_isrAdicional]
    ,[imp_Imss]
    ,[imp_Infonavit]
    ,[imp_subTotal]
    ,[imp_total]
    FROM [nomina_rinku].[dbo].[mae_nominas]
    WHERE
        [num_mes] = @num_mes
        AND [num_anio] = @num_anio
END
GO

USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_consultarRoles]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_consultarRoles]
END
GO

CREATE PROCEDURE [dbo].[proc_consultarRoles]
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Retorna los diversos roles
-- Versión: 20190307.1032
-- =============================================
BEGIN
    SELECT
        [idu_rol]
        ,[des_rol]
    FROM [dbo].[cat_roles]
END
GO
USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_consultarTiposEmpleados]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_consultarTiposEmpleados]
END
GO

CREATE PROCEDURE [dbo].[proc_consultarTiposEmpleados]
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Retorna los tipos de empleados
-- Versión: 20190307.1032
-- =============================================
BEGIN
    SELECT
        [idu_tipoEmpleado]
        ,[des_tipoEmpleado]
    FROM [dbo].[cat_tiposEmpleados]
END
GO

USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_consultarTurnos]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_consultarTurnos]
END
GO

CREATE PROCEDURE [dbo].[proc_consultarTurnos]
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Retorna los diversos Turnos
-- Versión: 20190307.1032
-- =============================================
BEGIN
    SELECT
        [idu_turno]
        ,[des_turno]
    FROM [nomina_rinku].[dbo].[cat_turnos]
END
GO

USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('nomina_rinku.dbo.proc_corteMensual', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.proc_corteMensual
END
GO

CREATE PROCEDURE dbo.proc_corteMensual
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description: Se encarga de realizar el corte mensual
-- Versión: 20190307.0841
-- =============================================
BEGIN
    DECLARE	@fecha date = GETDATE();
    DECLARE @fechaCorte date;
    SELECT @fechaCorte =
        DateAdd(DAY, 21 - 1,
        DateAdd(MONTH, MONTH(@fecha) - 1,
        DateAdd(YEAR, YEAR(@fecha)-1900, 0)))

    INSERT INTO nomina_rinku.dbo.mae_nominas
        (num_empleado
        ,fec_corte
        ,idu_estatus
        ,num_mes
        ,num_anio
        ,imp_base
        ,imphrsExtra
        ,imp_entregas
        ,imp_bonos
        ,imp_isr
        ,imp_isrAdicional
        ,imp_Imss
        ,imp_Infonavit
        ,imp_subTotal
        ,imp_total)
    SELECT
        num_empleado AS num_empleado
        ,@fechaCorte AS fec_corte
        ,0 AS idu_estatus
        ,MONTH(@fechaCorte) AS num_mes
        ,YEAR(@fechaCorte) AS num_anio
        ,SUM(hrsTrabajadas) AS imp_base
        ,SUM(hrsExtras) AS imphrsExtra
        ,SUM(numEntregas) AS imp_entregas
        ,0 AS imp_bonos
        ,0 AS imp_isr
        ,0 AS imp_isrAdicional
        ,0 AS imp_Imss
        ,0 AS imp_Infonavit
        ,0 AS imp_subTotal
        ,0 AS imp_total
    FROM nomina_rinku.dbo.mov_nominaDetalles
    WHERE
        clv_procesado = 0
        AND fec_movimiento < @fechaCorte
        GROUP BY num_empleado

    -------------------------Ingresos-------------------------
    --Salario Base
    UPDATE nomina SET
    --SELECT
        imp_base = (nomina.imp_base * emp.clv_valor)
    FROM nomina_rinku.dbo.mae_nominas nomina
    INNER JOIN vis_empleados emp ON emp.num_empleado = nomina.num_empleado
    WHERE
        nomina.Idu_estatus = 0
        AND idu_detalle = 1

    --Salario por Entregas
    UPDATE nomina SET
    --SELECT
        imp_entregas = (nomina.imp_entregas * emp.clv_valor)
    FROM nomina_rinku.dbo.mae_nominas nomina
    INNER JOIN vis_empleados emp ON emp.num_empleado = nomina.num_empleado
    WHERE
        nomina.Idu_estatus = 0
        AND idu_detalle = 2

    --Salario por Bono
    UPDATE nomina SET
    --SELECT
        imp_bonos = (nomina.imp_bonos * emp.clv_valor)
    FROM nomina_rinku.dbo.mae_nominas nomina
    INNER JOIN vis_empleados emp ON emp.num_empleado = nomina.num_empleado
    WHERE
        nomina.Idu_estatus = 0
        AND idu_detalle = 3
        AND idu_tipoEmpleado = 1

    --SubTotal
    UPDATE nomina SET
    --SELECT
        imp_total = imp_base +((imp_entregas + imp_bonos) + imphrsExtra)
    FROM nomina_rinku.dbo.mae_nominas nomina
    INNER JOIN cat_empleadosDetalles emp ON emp.num_empleado = nomina.num_empleado
    WHERE
        nomina.Idu_estatus = 0
    -------------------------Egresos-------------------------
    --ISR
    UPDATE nomina SET
    --SELECT
        imp_isr = nomina.imp_subTotal * (CAST(conf.clv_valor as FLOAT))
    FROM nomina_rinku.dbo.mae_nominas nomina
    INNER JOIN cat_empleadosDetalles emp ON emp.num_empleado = nomina.num_empleado
    INNER JOIN cat_confMonetarias conf ON conf.idu_confMonetaria = 10
    WHERE
        nomina.Idu_estatus = 0
    --ISR Adicional
    UPDATE nomina SET
    --SELECT
        imp_isrAdicional = nomina.imp_subTotal * (CAST(conf.clv_valor as FLOAT))
    FROM nomina_rinku.dbo.mae_nominas nomina
    INNER JOIN cat_empleadosDetalles emp ON emp.num_empleado = nomina.num_empleado
    INNER JOIN cat_confMonetarias conf ON conf.idu_confMonetaria = 11
    WHERE
        nomina.Idu_estatus = 0
        AND nomina.imp_subTotal > 16000

    -------------------------Procesos Adicionales-------------------------
    --Hora Extra
    IF EXISTS (SELECT TOP 1 1 FROM cat_confSistemas
                WHERE idu_confSistemas = 1 AND opc_activo=1)
    BEGIN
        UPDATE nomina SET
        --SELECT
            imphrsExtra = (nomina.imphrsExtra * (1 + CAST(emp.clv_valor AS FLOAT)))
        FROM nomina_rinku.dbo.mae_nominas nomina
        INNER JOIN vis_empleados emp ON emp.num_empleado = nomina.num_empleado
        WHERE
            nomina.Idu_estatus = 0
            AND idu_detalle = 4
            AND idu_tipoEmpleado = 1
    END
    --IMSS
    IF EXISTS (SELECT TOP 1 1 FROM cat_confSistemas
                WHERE idu_confSistemas = 2 AND opc_activo = 1)
    BEGIN
        UPDATE nomina SET
        --SELECT
            imp_Imss = nomina.imp_subTotal * (CAST(conf.clv_valor as FLOAT))
        FROM nomina_rinku.dbo.mae_nominas nomina
        INNER JOIN cat_empleadosDetalles emp ON emp.num_empleado = nomina.num_empleado
        INNER JOIN cat_confMonetarias conf ON conf.idu_confMonetaria = 13
        WHERE
            nomina.Idu_estatus = 0
            AND emp.clv_imss = 1
    END
    --Infonavit
    IF EXISTS (SELECT TOP 1 1 FROM cat_confSistemas
                WHERE idu_confSistemas = 3 AND opc_activo = 1)
    BEGIN
        UPDATE nomina SET
        --SELECT
            imp_Infonavit = nomina.imp_subTotal * (CAST(clv_valor AS FLOAT))
        FROM nomina_rinku.dbo.mae_nominas nomina
        INNER JOIN cat_empleadosDetalles emp ON emp.num_empleado = nomina.num_empleado
        INNER JOIN cat_confMonetarias conf ON conf.idu_confMonetaria = 14
        WHERE
            nomina.Idu_estatus = 0
            AND emp.clv_infonavit = 1
    END
    -------------------------Finalizar Proceso-------------------------
    UPDATE nomina SET
    --SELECT
        imp_total = imp_subTotal - ((imp_isr + imp_isrAdicional) + imp_Imss + imp_Infonavit)
    FROM nomina_rinku.dbo.mae_nominas nomina
    WHERE
        nomina.Idu_estatus = 0

    UPDATE mov SET
    --SELECT
        clv_procesado = 1
    FROM nomina_rinku.dbo.mov_nominaDetalles mov

    UPDATE nomina SET
    --SELECT
        idu_estatus = 1
    FROM nomina_rinku.dbo.mae_nominas nomina
END
GO
USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_guardarConfigMonetaria]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_guardarConfigMonetaria]
END
GO

CREATE PROCEDURE [dbo].[proc_guardarConfigMonetaria]
    (@idu_confMonetaria INT
    ,@clv_valor VARCHAR(50))
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	sp para actualizar valores configurables
-- Versión: 20190307.0841
-- =============================================
BEGIN
    UPDATE [nomina_rinku].[dbo].[cat_confMonetarias]
    SET [clv_valor] = @clv_valor
    WHERE [idu_confMonetaria] = @idu_confMonetaria
END
GO
USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_guardarConfigSistema]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_guardarConfigSistema]
END
GO

CREATE PROCEDURE [dbo].[proc_guardarConfigSistema]
    (@idu_confSistemas INT
    ,@opc_activo CHAR(1))
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Actualiza las opciones especiales del sistema
-- Versión: 20190307.0841
-- =============================================
BEGIN
    UPDATE [nomina_rinku].[dbo].[cat_confSistemas]
    SET [opc_activo] = @opc_activo
    WHERE
        [idu_confSistemas] = @idu_confSistemas
END
GO
USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_guardarEmpleado]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_guardarEmpleado]
END
GO

CREATE PROCEDURE [dbo].[proc_guardarEmpleado]
    (@num_empleado int
    ,@nom_empleado varchar(50)
    ,@idu_tipoEmpleado int
    ,@idu_rol int
    ,@clv_imss bit
    ,@clv_infonavit bit)
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Guarda un empleado nuevo si no existe,
-- si ya existe actualiza sus datos.
-- Versión: 20190307.0841
-- =============================================
BEGIN
    UPDATE [nomina_rinku].[dbo].[cat_empleados]
    SET [nom_empleado] = @nom_empleado
    WHERE num_empleado = @num_empleado

    UPDATE [nomina_rinku].[dbo].[cat_empleadosDetalles]
    SET [idu_tipoEmpleado] = @idu_tipoEmpleado
        ,[idu_rol] = @idu_rol
        ,[clv_imss] = @clv_imss
        ,[clv_infonavit] = @clv_infonavit
    WHERE [num_empleado] = @num_empleado

    IF(@@ROWCOUNT = 0)
    BEGIN
        INSERT INTO [nomina_rinku].[dbo].[cat_empleados]
            ([num_empleado] ,[nom_empleado])
        VALUES
            (@num_empleado ,@nom_empleado);

        INSERT INTO [dbo].[cat_empleadosDetalles]
            ([num_empleado] ,[idu_tipoEmpleado] ,[idu_rol] ,[clv_imss] ,[clv_infonavit])
        VALUES
            (@num_empleado
            ,@idu_tipoEmpleado
            ,@idu_rol
            ,@clv_imss
            ,@clv_infonavit);
    END
END
GO
USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_guardarMovimientos]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_guardarMovimientos]
END
GO

CREATE PROCEDURE [dbo].[proc_guardarMovimientos]
    (@num_empleado INT
    ,@fec_movimiento DATE
    ,@hrsTrabajadas INT
    ,@hrsExtras INT
    ,@numEntregas INT
    ,@idu_turno INT)
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Guarda un movimiento nuevo si no existe,
-- si ya existe actualiza sus datos.
-- Versión: 20190307.0841
-- =============================================
BEGIN
    UPDATE [nomina_rinku].[dbo].[mov_nominaDetalles]
    SET  [num_empleado] = @num_empleado
        ,[fec_movimiento] = @fec_movimiento
        ,[hrsTrabajadas] = @hrsTrabajadas
        ,[hrsExtras] = @hrsExtras
        ,[numEntregas] = @numEntregas
        ,[idu_turno] = @idu_turno
    WHERE   [num_empleado] = @num_empleado
        AND [fec_movimiento] = @fec_movimiento;

    IF(@@ROWCOUNT = 0)
    BEGIN
        INSERT INTO [nomina_rinku].[dbo].[mov_nominaDetalles]
            ([num_empleado]
            ,[fec_movimiento]
            ,[hrsTrabajadas]
            ,[hrsExtras]
            ,[numEntregas]
            ,[idu_turno])
        VALUES
            (@num_empleado
            ,@fec_movimiento
            ,@hrsTrabajadas
            ,@hrsExtras
            ,@numEntregas
            ,@idu_turno);
    END
END
GO
USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_nuevoEmpleado]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_nuevoEmpleado]
END
GO

CREATE PROCEDURE [dbo].[proc_nuevoEmpleado]
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	Retorna el id maximo +1
-- Versión: 20190307.0901
-- =============================================
BEGIN
    SELECT
        (ISNULL(MAX(num_empleado), 0) + 1) AS num_empleado
    FROM cat_empleados
END
GO