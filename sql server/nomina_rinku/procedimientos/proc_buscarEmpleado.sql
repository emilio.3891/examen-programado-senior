USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_buscarEmpleado]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_buscarEmpleado]
END
GO

CREATE PROCEDURE [dbo].[proc_buscarEmpleado]
    (@num_empleado INT
    ,@nom_empleado VARCHAR(50))
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:
-- Versión: 20190307.0841
-- =============================================
BEGIN
    IF (@num_empleado IS NULL)
    BEGIN
        SELECT
            [emp].[num_empleado],
            [emp].[nom_empleado],
            [det].[idu_tipoEmpleado],
            [det].[idu_rol],
            [det].[clv_imss],
            [det].[clv_infonavit]
        FROM [cat_empleados] emp
        INNER JOIN [cat_empleadosDetalles] det
            ON [emp].[num_empleado] = [det].[num_empleado]
        WHERE [emp].[nom_empleado] LIKE '%' + @nom_empleado + '%';
    END
    ELSE
    BEGIN
        SELECT
            [emp].[num_empleado],
            [emp].[nom_empleado],
            [det].[idu_tipoEmpleado],
            [det].[idu_rol],
            [det].[clv_imss],
            [det].[clv_infonavit]
        FROM [cat_empleados] emp
        INNER JOIN [cat_empleadosDetalles] det
            ON [emp].[num_empleado] = [det].[num_empleado]
        WHERE [emp].[num_empleado] = @num_empleado;
    END
END
GO
