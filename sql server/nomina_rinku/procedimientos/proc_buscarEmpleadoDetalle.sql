USE nomina_rinku
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('[nomina_rinku].[dbo].[proc_buscarEmpleadoDetalle]', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[proc_buscarEmpleadoDetalle]
END
GO

CREATE PROCEDURE [dbo].[proc_buscarEmpleadoDetalle]
    (@num_empleado INT)
WITH EXECUTE AS OWNER
AS
-- =============================================
-- Author: Jesús Emilio Reyes Soto
-- Base de Datos: nomina_rinku
-- Create date: 2019/03/07
-- Description:	
-- Versión: 20190307.1032
-- =============================================
BEGIN
    SELECT DISTINCT 
        [num_empleado]
        ,[nom_empleado]
        ,[idu_tipoEmpleado]
        ,[des_tipoEmpleado]
        ,[idu_rol]
        ,[des_rol]
    FROM [nomina_rinku].[dbo].[vis_empleados]
    WHERE [num_empleado] = @num_empleado
END
GO
