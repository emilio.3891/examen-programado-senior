USE [nomina_rinku]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vis_empleados]
AS
SELECT
    dbo.cat_empleados.num_empleado, dbo.cat_empleadosDetalles.clv_imss, 
    dbo.cat_empleadosDetalles.clv_infonavit, dbo.cat_empleados.nom_empleado, 
    dbo.cat_tiposEmpleados.idu_tipoEmpleado, dbo.cat_tiposEmpleados.des_tipoEmpleado,
    dbo.cat_roles.des_rol, dbo.cat_rolesDetalles.idu_rol, dbo.cat_rolesDetalles.idu_detalle, 
    dbo.cat_confMonetarias.idu_confMonetaria, dbo.cat_confMonetarias.clv_valor
FROM dbo.cat_empleados 
INNER JOIN dbo.cat_empleadosDetalles ON dbo.cat_empleados.num_empleado = dbo.cat_empleadosDetalles.num_empleado 
INNER JOIN dbo.cat_tiposEmpleados ON dbo.cat_empleadosDetalles.idu_tipoEmpleado = dbo.cat_tiposEmpleados.idu_tipoEmpleado 
INNER JOIN dbo.cat_roles ON dbo.cat_empleadosDetalles.idu_rol = dbo.cat_roles.idu_rol 
INNER JOIN dbo.cat_rolesDetalles ON dbo.cat_roles.idu_rol = dbo.cat_rolesDetalles.idu_rol 
INNER JOIN dbo.cat_confMonetarias ON dbo.cat_rolesDetalles.idu_confMonetaria = dbo.cat_confMonetarias.idu_confMonetaria
GO

USE [nomina_rinku]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vis_roles]
AS
SELECT
    dbo.cat_roles.idu_rol, dbo.cat_roles.des_rol, dbo.cat_rolesDetalles.idu_detalle, 
    dbo.cat_rolesDetalles.idu_confMonetaria, dbo.cat_confMonetarias.clv_valor, 
    dbo.cat_confMonetarias.des_confMonetaria
FROM dbo.cat_roles 
INNER JOIN dbo.cat_rolesDetalles ON dbo.cat_roles.idu_rol = dbo.cat_rolesDetalles.idu_rol 
INNER JOIN dbo.cat_confMonetarias ON dbo.cat_rolesDetalles.idu_confMonetaria = dbo.cat_confMonetarias.idu_confMonetaria
GO


